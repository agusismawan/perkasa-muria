<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Persediaan extends My_Controller
{
     protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Persediaan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $persediaan = $this->Persediaan_model->get_all();

        $title = array(
            'title' => 'persediaan',
        );

        $data = array(
            'persediaan_data' => $persediaan,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('persediaan/tbl_persediaan_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id) 
    {
        $row = $this->Persediaan_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_persediaan' => $row->id_persediaan,
		'id_barang' => $row->id_barang,
		'persediaan_awal' => $row->persediaan_awal,
		'persediaan' => $row->persediaan,
		'jumlah_masuk' => $row->jumlah_masuk,
		'id_barang_masuk' => $row->id_barang_masuk,
	    );

            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('persediaan/tbl_persediaan_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persediaan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('persediaan/create_action'),
	    'id_persediaan' => set_value('id_persediaan'),
	    'id_barang' => set_value('id_barang'),
	    'persediaan_awal' => set_value('persediaan_awal'),
	    'persediaan' => set_value('persediaan'),
	    'jumlah_masuk' => set_value('jumlah_masuk'),
	    'id_barang_masuk' => set_value('id_barang_masuk'),
	);
         $title = array(
            'title' => 'Detail',
            );
        $this->load->view('cover/header', $title);
        $this->load->view('persediaan/tbl_persediaan_form', $data);
        $this->load->view('cover/footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_barang' => $this->input->post('id_barang',TRUE),
		'persediaan_awal' => $this->input->post('persediaan_awal',TRUE),
		'persediaan' => $this->input->post('persediaan',TRUE),
		'jumlah_masuk' => $this->input->post('jumlah_masuk',TRUE),
		'id_barang_masuk' => $this->input->post('id_barang_masuk',TRUE),
	    );
        
            $this->Persediaan_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('persediaan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Persediaan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('persediaan/update_action'),
		'id_persediaan' => set_value('id_persediaan', $row->id_persediaan),
		'id_barang' => set_value('id_barang', $row->id_barang),
		'persediaan_awal' => set_value('persediaan_awal', $row->persediaan_awal),
		'persediaan' => set_value('persediaan', $row->persediaan),
		'jumlah_masuk' => set_value('jumlah_masuk', $row->jumlah_masuk),
		'id_barang_masuk' => set_value('id_barang_masuk', $row->id_barang_masuk),
	    );
            
            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('persediaan/tbl_persediaan_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persediaan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_persediaan', TRUE));
        } else {
            $data = array(
		'id_barang' => $this->input->post('id_barang',TRUE),
		'persediaan_awal' => $this->input->post('persediaan_awal',TRUE),
		'persediaan' => $this->input->post('persediaan',TRUE),
		'jumlah_masuk' => $this->input->post('jumlah_masuk',TRUE),
		'id_barang_masuk' => $this->input->post('id_barang_masuk',TRUE),
	    );

            $this->Persediaan_model->update($this->input->post('id_persediaan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('persediaan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Persediaan_model->get_by_id($id);

        if ($row) {
            $this->Persediaan_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('persediaan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('persediaan'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_barang', 'id barang', 'trim|required');
	$this->form_validation->set_rules('persediaan_awal', 'persediaan awal', 'trim|required');
	$this->form_validation->set_rules('persediaan', 'persediaan', 'trim|required');
	$this->form_validation->set_rules('jumlah_masuk', 'jumlah masuk', 'trim|required');
	$this->form_validation->set_rules('id_barang_masuk', 'id barang masuk', 'trim|required');

	$this->form_validation->set_rules('id_persediaan', 'id_persediaan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_persediaan.xls";
        $judul = "tbl_persediaan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Barang");
	xlsWriteLabel($tablehead, $kolomhead++, "Persediaan Awal");
	xlsWriteLabel($tablehead, $kolomhead++, "Persediaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Masuk");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Barang Masuk");

	foreach ($this->Persediaan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_barang);
	    xlsWriteNumber($tablebody, $kolombody++, $data->persediaan_awal);
	    xlsWriteNumber($tablebody, $kolombody++, $data->persediaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->jumlah_masuk);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_barang_masuk);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tbl_persediaan.doc");

        $data = array(
            'tbl_persediaan_data' => $this->Persediaan_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('persediaan/tbl_persediaan_doc',$data);
    }

}