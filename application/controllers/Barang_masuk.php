<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_masuk extends My_Controller
{
    protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_masuk_model');
        $this->load->model('Barang_model');
        $this->load->model('Persediaan_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $barang_masuk = $this->Barang_masuk_model->get_all();

        $title = array(
            'title' => 'barang_masuk',
        );

        $data = array(
            'barang_masuk_data' => $barang_masuk,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('barang_masuk/barang_masuk_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);
        $barang = $this->Barang_masuk_model->get_by_id_detail($id);
        if ($row) {
            $data = array(
                'id_barang_masuk' => $id,
                'tgl_masuk' => $row->tgl_masuk,
                'keterangan_masuk' => $row->keterangan_masuk,
                'id_user' => $row->id_user,
                'barang_data' => $barang,
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang_masuk/barang_masuk_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_masuk'));
        }
    }


    public function create($id_barang_masuk = '')
    {
        date_default_timezone_set("Asia/Jakarta");
        $id =  "IN-" . date("ymd", strtotime(date('Y-m-d'))) . date('Hi');
        if ($id_barang_masuk == '' or $id_barang_masuk == null) {
            $data = array(
                'button' => 'Create',
                'action' => site_url('barang_masuk/create/' . $id),
                'id_barang_masuk' => $id_barang_masuk,
                'tgl_masuk' => set_value('tgl_masuk'),
                'keterangan_masuk' => set_value('keterangan_masuk'),
                'id_user' => set_value('id_user'),
            );
            $title = array(
                'title' => 'Detail',
            );

            $this->load->view('cover/header', $title);
            $this->load->view('barang_masuk/barang_masuk_form', $data);
            $this->load->view('cover/footer');
        } else {
            $barang = $this->Barang_model->get_all();
            $data = array(
                'button' => 'Create',
                'action' => site_url('barang_masuk/create_action'),
                'id_barang_masuk' => $id_barang_masuk,
                'tgl_masuk' => $this->input->post('tgl_masuk'),
                'keterangan_masuk' => $this->input->post('keterangan_masuk'),
                'barang_data' => $barang,
            );
            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang_masuk/barang_masuk_tambah', $data);
            $this->load->view('cover/footer');
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_barang_masuk' => $this->input->post('id_masuk', TRUE),
                'tgl_masuk' => $this->input->post('tgl_masuk', TRUE),
                'keterangan_masuk' => $this->input->post('keterangan_masuk', TRUE),
                'id_user' => $this->session->userdata('id_user'),
            );

            $id_barang = $this->input->post('id_barang');
            $persediaan = $this->input->post('persediaan');
            $jumlah = $this->input->post('jumlah');

            $cek_jumlah = 0;
            foreach ($jumlah as $key) {
                $cek_jumlah = $cek_jumlah + $key;
            }

            if ($cek_jumlah <= 0) {
                echo "<script type='text/javascript'>alert('Submit Gagal!')</script>";
                redirect(site_url('barang_masuk'), 'refresh');
            }

            $this->db->trans_start();
            $this->Barang_masuk_model->insert($data);
            foreach ($id_barang as $key => $value) {
                if ($jumlah[$key] > 0) {
                    $insert_data = array(
                        'id_barang' => $id_barang[$key],
                        'persediaan_awal' => $persediaan[$key],
                        'persediaan' => $jumlah[$key],
                        'jumlah_masuk' => $jumlah[$key],
                        'id_barang_masuk' => $this->input->post('id_masuk'),
                    );
                    $this->Persediaan_model->insert($insert_data);
                }
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang_masuk'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang_masuk/update_action'),
                'id_barang_masuk' => set_value('id_barang_masuk', $row->id_barang_masuk),
                'tgl_masuk' => set_value('tgl_masuk', $row->tgl_masuk),
                'keterangan_masuk' => set_value('keterangan_masuk', $row->keterangan_masuk),
                'id_user' => set_value('id_user', $row->id_user),
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang_masuk/barang_masuk_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_masuk'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_barang_masuk', TRUE));
        } else {
            $data = array(
                'tgl_masuk' => $this->input->post('tgl_masuk', TRUE),
                'keterangan_masuk' => $this->input->post('keterangan_masuk', TRUE),
            );

            $this->Barang_masuk_model->update($this->input->post('id_barang_masuk', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang_masuk'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_masuk_model->get_by_id($id);

        if ($row) {
            $this->Barang_masuk_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang_masuk'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_masuk'));
        }
    }

    public function cetak()
    {
        $id = $this->input->post('id_masuk');
        $row = $this->Barang_masuk_model->get_by_id($id);
        $barang = $this->Barang_masuk_model->get_by_id_detail($id);
        if ($row) {
            $data = array(
                'id_barang_masuk' => $id,
                'tgl_masuk' => $row->tgl_masuk,
                'keterangan_masuk' => $row->keterangan_masuk,
                'id_user' => $row->id_user,
                'laporan_data' => $barang,
            );
            $data['laporan_data'] = $this->Barang_masuk_model->get_by_id_detail($id);
            $this->load->view('barang_masuk/laporan_masuk', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_masuk'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('tgl_masuk', 'tgl masuk', 'trim|required');
        $this->form_validation->set_rules('keterangan_masuk', 'keterangan masuk', 'trim|required');
        $this->form_validation->set_rules('id_user', 'id user', 'trim');
        $this->form_validation->set_rules('id_barang_masuk', 'id_barang_masuk', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang_masuk.xls";
        $judul = "barang_masuk";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Tgl Masuk");
        xlsWriteLabel($tablehead, $kolomhead++, "Keterangan Masuk");
        xlsWriteLabel($tablehead, $kolomhead++, "Id User");

        foreach ($this->Barang_masuk_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_masuk);
            xlsWriteLabel($tablebody, $kolombody++, $data->keterangan_masuk);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_user);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=barang_masuk.doc");

        $data = array(
            'barang_masuk_data' => $this->Barang_masuk_model->get_all(),
            'start' => 0
        );

        $this->load->view('barang_masuk/barang_masuk_doc', $data);
    }
}
