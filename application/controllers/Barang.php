<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang extends My_Controller
{
    protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $barang = $this->Barang_model->get_all();

        $title = array(
            'title' => 'barang',
        );

        $data = array(
            'barang_data' => $barang,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('barang/barang_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id)
    {
        $row = $this->Barang_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id_barang' => $row->id_barang,
                'id_kategori' => $row->id_kategori,
                'nama_barang' => $row->nama_barang,
                'kode_barang' => $row->kode_barang,
                // 'satuan' => $row->satuan,
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang/barang_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function detail()
    {
        $this->load->view('barang/view_detail');
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('barang/create_action'),
            'id_barang' => set_value('id_barang'),
            'kategori' => $this->db->get('tbl_kategori'),
            'nama_barang' => set_value('nama_barang'),
            'kode_barang' => set_value('kode_barang'),
            // 'satuan' => set_value('satuan'),
        );

        $this->load->view('cover/header');
        $this->load->view('barang/barang_form', $data);
        $this->load->view('cover/footer');
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'id_kategori' => $this->input->post('id_kategori', TRUE),
                'nama_barang' => $this->input->post('nama_barang', TRUE),
                'kode_barang' => $this->input->post('kode_barang', TRUE),
                // 'satuan' => $this->input->post('satuan',TRUE),
            );

            $this->Barang_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang/update_action'),
                'id_barang' => set_value('id_barang', $row->id_barang),
                'kategori' => $this->db->get('tbl_kategori'),
                'nama_barang' => set_value('nama_barang', $row->nama_barang),
                'kode_barang' => set_value('kode_barang', $row->kode_barang),
                // 'satuan' => set_value('satuan', $row->satuan),
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang/barang_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_barang', TRUE));
        } else {
            $data = array(
                'id_kategori' => $this->input->post('id_kategori', TRUE),
                'nama_barang' => $this->input->post('nama_barang', TRUE),
                'kode_barang' => $this->input->post('kode_barang', TRUE),
                // 'satuan' => $this->input->post('satuan',TRUE),
            );

            $this->Barang_model->update($this->input->post('id_barang', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_model->get_by_id($id);

        if ($row) {
            $this->Barang_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('id_kategori', 'id kategori', 'trim|required');
        $this->form_validation->set_rules('nama_barang', 'nama barang', 'trim|required');
        $this->form_validation->set_rules('kode_barang', 'kode barang', 'trim|required');
        // $this->form_validation->set_rules('satuan', 'satuan', 'trim|required');

        $this->form_validation->set_rules('id_barang', 'id_barang', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang.xls";
        $judul = "barang";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Kategori");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama Barang");
        xlsWriteLabel($tablehead, $kolomhead++, "Kode Barang");

        foreach ($this->Barang_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_kategori);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_barang);
            xlsWriteLabel($tablebody, $kolombody++, $data->kode_barang);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=barang.doc");

        $data = array(
            'barang_data' => $this->Barang_model->get_all(),
            'start' => 0
        );

        $this->load->view('barang/barang_doc', $data);
    }
}
