<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pemakaian extends My_Controller
{
     protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pemakaian_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $pemakaian = $this->Pemakaian_model->get_all();

        $title = array(
            'title' => 'pemakaian',
        );

        $data = array(
            'pemakaian_data' => $pemakaian,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('pemakaian/tbl_pemakaian_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id) 
    {
        $row = $this->Pemakaian_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id_pemakaian' => $row->id_pemakaian,
		'id_persediaan' => $row->id_persediaan,
		'jumlah_keluar' => $row->jumlah_keluar,
		'id_barang_keluar' => $row->id_barang_keluar,
	    );

            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('pemakaian/tbl_pemakaian_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemakaian'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('pemakaian/create_action'),
	    'id_pemakaian' => set_value('id_pemakaian'),
	    'id_persediaan' => set_value('id_persediaan'),
	    'jumlah_keluar' => set_value('jumlah_keluar'),
	    'id_barang_keluar' => set_value('id_barang_keluar'),
	);
         $title = array(
            'title' => 'Detail',
            );
        $this->load->view('cover/header', $title);
        $this->load->view('pemakaian/tbl_pemakaian_form', $data);
        $this->load->view('cover/footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'id_persediaan' => $this->input->post('id_persediaan',TRUE),
		'jumlah_keluar' => $this->input->post('jumlah_keluar',TRUE),
		'id_barang_keluar' => $this->input->post('id_barang_keluar',TRUE),
	    );
        
            $this->Pemakaian_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pemakaian'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Pemakaian_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('pemakaian/update_action'),
		'id_pemakaian' => set_value('id_pemakaian', $row->id_pemakaian),
		'id_persediaan' => set_value('id_persediaan', $row->id_persediaan),
		'jumlah_keluar' => set_value('jumlah_keluar', $row->jumlah_keluar),
		'id_barang_keluar' => set_value('id_barang_keluar', $row->id_barang_keluar),
	    );
            
            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('pemakaian/tbl_pemakaian_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemakaian'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_pemakaian', TRUE));
        } else {
            $data = array(
		'id_persediaan' => $this->input->post('id_persediaan',TRUE),
		'jumlah_keluar' => $this->input->post('jumlah_keluar',TRUE),
		'id_barang_keluar' => $this->input->post('id_barang_keluar',TRUE),
	    );

            $this->Pemakaian_model->update($this->input->post('id_pemakaian', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pemakaian'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Pemakaian_model->get_by_id($id);

        if ($row) {
            $this->Pemakaian_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pemakaian'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pemakaian'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_persediaan', 'id persediaan', 'trim|required');
	$this->form_validation->set_rules('jumlah_keluar', 'jumlah keluar', 'trim|required');
	$this->form_validation->set_rules('id_barang_keluar', 'id barang keluar', 'trim|required');

	$this->form_validation->set_rules('id_pemakaian', 'id_pemakaian', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_pemakaian.xls";
        $judul = "tbl_pemakaian";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Persediaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Keluar");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Barang Keluar");

	foreach ($this->Pemakaian_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_persediaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->jumlah_keluar);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_barang_keluar);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=tbl_pemakaian.doc");

        $data = array(
            'tbl_pemakaian_data' => $this->Pemakaian_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('pemakaian/tbl_pemakaian_doc',$data);
    }

}