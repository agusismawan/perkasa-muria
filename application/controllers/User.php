<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends My_Controller
{
    protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->library('form_validation');
        $this->load->library('encryption');
    }

    public function index()
    {
        $user = $this->User_model->get_all();

        $title = array(
            'title' => 'user',
        );

        $data = array(
            'user_data' => $user,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('user/user_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id)
    {
        $key = 'rahasia';
        $row = $this->User_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id_user' => $row->id_user,
                'nip' => $row->nip,
                'nama_user' => $row->nama_user,
                'username' => $row->username,
                'password' => $row->password,
                'role' => $row->role,
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('user/user_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function create()
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('user/create_action'),
            'id_user' => set_value('id_user'),
            'nip' => set_value('nip'),
            'nama_user' => set_value('nama_user'),
            'username' => set_value('username'),
            'password' => set_value('password'),
            'role' => set_value('role'),
        );
        $title = array(
            'title' => 'Detail',
        );
        $this->load->view('cover/header', $title);
        $this->load->view('user/user_form', $data);
        $this->load->view('cover/footer');
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'nip' => $this->input->post('nip', TRUE),
                'nama_user' => $this->input->post('nama_user', TRUE),
                'username' => $this->input->post('username', TRUE),
                'password' => sha1($this->input->post('password', TRUE)),
                'role' => $this->input->post('role', TRUE),
            );

            $this->User_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('user'));
        }
    }

    public function update($id)
    {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('user/update_action'),
                'id_user' => set_value('id_user', $row->id_user),
                'nip' => set_value('nip', $row->nip),
                'nama_user' => set_value('nama_user', $row->nama_user),
                'username' => set_value('username', $row->username),
                'password' => set_value('password', $row->password),
                'role' => set_value('role', $row->role),
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('user/user_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_user', TRUE));
        } else {
            $data = array(
                'nip' => $this->input->post('nip', TRUE),
                'nama_user' => $this->input->post('nama_user', TRUE),
                'username' => $this->input->post('username', TRUE),
                'password' => sha1($this->input->post('password', TRUE)),
                'role' => $this->input->post('role', TRUE),
            );

            $this->User_model->update($this->input->post('id_user', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('user'));
        }
    }

    public function delete($id)
    {
        $row = $this->User_model->get_by_id($id);

        if ($row) {
            $this->User_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('user'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('user'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('nama_user', 'nama user', 'trim|required');
        $this->form_validation->set_rules('username', 'username', 'trim|required');
        $this->form_validation->set_rules('password', 'password', 'trim|required');
        $this->form_validation->set_rules('role', 'role', 'trim|required');

        $this->form_validation->set_rules('id_user', 'id_user', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "user.xls";
        $judul = "user";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Nip");
        xlsWriteLabel($tablehead, $kolomhead++, "Nama User");
        xlsWriteLabel($tablehead, $kolomhead++, "Username");
        xlsWriteLabel($tablehead, $kolomhead++, "Password");
        xlsWriteLabel($tablehead, $kolomhead++, "Role");

        foreach ($this->User_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->nip);
            xlsWriteLabel($tablebody, $kolombody++, $data->nama_user);
            xlsWriteLabel($tablebody, $kolombody++, $data->username);
            xlsWriteLabel($tablebody, $kolombody++, $data->password);
            xlsWriteLabel($tablebody, $kolombody++, $data->role);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=user.doc");

        $data = array(
            'user_data' => $this->User_model->get_all(),
            'start' => 0
        );

        $this->load->view('user/user_doc', $data);
    }
}
