<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permintaan extends My_Controller
{
     protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_model');
        $this->load->model('Permintaan_model');
        $this->load->library('form_validation');
    }

     public function index()
    {
        $permintaan = $this->Permintaan_model->get_all();
        $title = array(
            'title' => 'permintaan',
        );

        $data = array(
            'permintaan_data' => $permintaan,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('permintaan/permintaan_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);
        $detail = $this->Permintaan_model->get_by_id_detail($id);
        if ($row) {
            $data = array(
		'id_permintaan' => $row->id_permintaan,
		'id_barang' => $detail->id_barang,
		'tgl_permintaan' => $row->tgl_permintaan,
		'jumlah_permintaan' => $detail->jumlah_permintaan,
		'keterangan_permintaan' => $row->keterangan_permintaan,    
        'id_user' => $row->id_user,
		'status' => $detail->status,
	    );

            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('permintaan/permintaan_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }

    public function details($bulan='', $tahun='')
    {           
        if ($this->session->userdata('role') == 'Kepala Gudang') {
        $row = $this->Permintaan_model->detail_permintaan_ka($bulan, $tahun);
        } else{
        $row = $this->Permintaan_model->detail_permintaan_kp($bulan, $tahun);
        }
        if ($row) {
            $data = array(
        'bulan' => $bulan,
        'tahun' => $tahun,
        'permintaan_data' => $row,
        );

            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('permintaan/permintaan_details', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }

    }

    public function create() 
    {
        $barang = $this->Barang_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('permintaan/create_action'),
	    'tgl_permintaan' => set_value('tgl_permintaan'),
	    'keterangan_permintaan' => set_value('keterangan_permintaan'),
        'id_user' => set_value('id_user'),
	    'barang_data' => $barang,
	);
         $title = array(
            'title' => 'Detail',
            );
        $this->load->view('cover/header', $title);
        $this->load->view('permintaan/permintaan_tambah', $data);
        $this->load->view('cover/footer');
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
        $id_barang = $this->input->post('id_barang');
        $jumlah = $this->input->post('jumlah');

        $cek_jumlah=0;
        foreach ($jumlah as $key) {
            $cek_jumlah = $cek_jumlah + $key;
        }

        if ($cek_jumlah <= 0 ) {
            echo "<script type='text/javascript'>alert('Submit Gagal!')</script>";
            redirect(site_url('permintaan'), 'refresh');
        } 

        $permintaan = array(
            'id_permintaan' => $this->input->post('id_permintaan'), 
            'tgl_permintaan' => $this->input->post('tgl_permintaan'), 
            'keterangan_permintaan' => $this->input->post('ket_permintaan'), 
            'id_user' => $this->session->userdata('id_user'), 
        );
        $this->db->trans_start();
        $this->Permintaan_model->insert($permintaan);
        foreach ($id_barang as $key => $value) {
            if ($jumlah[$key] > 0) {
            $data = array(
        'id_permintaan' => $this->input->post('id_permintaan'),
		'id_barang' => $id_barang[$key],
		'jumlah_permintaan' => $jumlah[$key],
        'status' => 'menunggu',
	    );

            $this->Permintaan_model->insert_detail($data);
            }

        }     
            $this->db->trans_complete();
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('permintaan'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('permintaan/update_action'),
		'id_permintaan' => set_value('id_permintaan', $row->id_permintaan),
		'tgl_permintaan' => set_value('tgl_permintaan', $row->tgl_permintaan),
		'keterangan_permintaan' => set_value('keterangan_permintaan', $row->keterangan_permintaan),
		'id_user' => set_value('id_user', $row->id_user),
	    );
            
            $title = array(
            'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('permintaan/permintaan_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_permintaan', TRUE));
        } else {
            $data = array(
		'tgl_permintaan' => $this->input->post('tgl_permintaan',TRUE),
		'keterangan_permintaan' => $this->input->post('keterangan_permintaan',TRUE),
	    );

            $this->Permintaan_model->update_permintaan($this->input->post('id_permintaan', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('permintaan'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Permintaan_model->get_by_id($id);

        if ($row) {
            $data = $this->Permintaan_model->detail_permintaan_kp_by_id($id);
            foreach ($data as $key) {
                if ($data->status != 'menunggu') {
                     $this->session->set_flashdata('message', 'Permintaan Tidak Dapat Dihapus');
                    redirect(site_url('permintaan'));
                }
            }
            $this->Permintaan_model->delete_detail($id);
            $this->Permintaan_model->delete($id);
            $this->session->set_flashdata('message', 'Permintaan Berhasil Dihapus');
            redirect(site_url('permintaan'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }

    public function cetak()
    {
        $bulan = $this->input->post('bulan');
        $tahun = $this->input->post('tahun');
        $id = $this->input->post('id_permintaan');
        $row = $this->Permintaan_model->detail_permintaan_kp_by_id($id);
        if ($row) {
            $data = array(
        'laporan_data' => $row,
        'id' => $id,
        );
        $this->load->view('permintaan/laporan_permintaan', $data);
    } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('permintaan'));
        }
    }

    public function detail()
    {
        $this->load->view('permintaan/view_detail');
    }

    public function update_status()
    {
        $id = $this->input->post('id');
        $status = $this->input->post('status');
        $data = array(
            'status' => $status, 
        );
        $this->Permintaan_model->update($id, $data);
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('id_barang', 'id barang', 'trim');
	$this->form_validation->set_rules('tgl_permintaan', 'tgl permintaan', 'trim');
	$this->form_validation->set_rules('jumlah_permintaan', 'jumlah permintaan', 'trim');
	$this->form_validation->set_rules('id_user', 'id user', 'trim');
	$this->form_validation->set_rules('id_permintaan', 'id_permintaan', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "permintaan.xls";
        $judul = "permintaan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Barang");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Permintaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Jumlah Permintaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Keterangan Permintaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Id User");

	foreach ($this->Permintaan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_barang);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_permintaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->jumlah_permintaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->keterangan_permintaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_user);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=permintaan.doc");

        $data = array(
            'permintaan_data' => $this->Permintaan_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('permintaan/permintaan_doc',$data);
    }

}