<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Main extends My_Controller {

		protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

	
	function __construct()
    {
        parent::__construct();
        $this->load->model('Main_model');
        $this->load->library('form_validation');
    }

	public function index()
	{
		date_default_timezone_set("Asia/Jakarta");
		$tgl = date("Y-m-d");
		$min = $this->Main_model->stock();
		$keluar = $this->Main_model->keluar($tgl);
		$last = $this->Main_model->last_out();
		$ln = $this->Main_model->last_nom();
		$data = array(
			'stok' => $min, 
			'nama_barang_min' => $min, 
			'keluar' => $keluar->jumlah,
			'last_id' => $last->id_barang_keluar,
			'last_jml' => $ln->jml,
		);

		$this->load->view('cover/header');
		$this->load->view('index', $data);
		$this->load->view('cover/footer');
	}

		public function laporan()
	{
		$data['list_laporan'] = $this->Main_model->laporan();
		$this->load->view('cover/header');
		$this->load->view('laporan', $data);
		$this->load->view('cover/footer');
	}

	public function details($thn='', $bln='')
	{
		$data = array();
		$data["bulan"] = $bln;
		$data["tahun"] = $thn;
		$data['laporan_data'] = $this->Main_model->lplpb($bln, $thn);
		$this->load->view('cover/header');
		$this->load->view('laporan_details', $data);
		$this->load->view('cover/footer');
	
	}

	public function cetak()
	{
		$bln = $this->input->post('bulan');
		$thn = $this->input->post('tahun');
		$tgl = date('Y-m-t', strtotime($thn.'/'.$bln.'/1'));
		$awal = $this->db->get('tbl_barang_masuk', 1)->row()->tgl_masuk;
		if ($tgl < $awal) {
			 $this->session->set_flashdata('message', 'Record not found');
            redirect(site_url('main/laporan'));
		}

		$data['laporan_data'] = $this->Main_model->lplpb($bln, $thn);
		
		$data["bulan"] = $bln;
		$data["tahun"] = $thn;
		$this->load->view('cetak', $data);
	}

	public function excel($thn='', $bln='')
    {
        $this->load->helper('exportexcel');
        $namaFile = "laporan.xls";
        $judul = "Laporan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Barang");
	xlsWriteLabel($tablehead, $kolomhead++, "Penerimaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Pemakaian");
	xlsWriteLabel($tablehead, $kolomhead++, "Permintaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Stok Awal");
	xlsWriteLabel($tablehead, $kolomhead++, "Persediaan");
	xlsWriteLabel($tablehead, $kolomhead++, "Sisa Stok");

	foreach ($this->Permintaan_model->lplpb($bln, $thn) as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->nama_barang);
	    xlsWriteNumber($tablebody, $kolombody++, $data->penerimaan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pemakaian);
	    xlsWriteNumber($tablebody, $kolombody++, $data->permintaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->stok_awal);
	    xlsWriteNumber($tablebody, $kolombody++, $data->persediaan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->sisa_stok);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }
}