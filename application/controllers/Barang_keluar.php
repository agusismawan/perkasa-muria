<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_keluar extends My_Controller
{
    protected $access = array('Pimpinan', 'Administrator', 'Kepala Gudang');

    function __construct()
    {
        parent::__construct();
        $this->load->model('Barang_keluar_model');
        $this->load->model('Barang_model');
        $this->load->model('Persediaan_model');
        $this->load->model('Pemakaian_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $barang_keluar = $this->Barang_keluar_model->get_all();
        $title = array(
            'title' => 'barang_keluar',
        );

        $data = array(
            'barang_keluar_data' => $barang_keluar,
        );
        $this->load->view('cover/header', $title);
        $this->load->view('barang_keluar/barang_keluar_list', $data);
        $this->load->view('cover/footer');
    }

    public function read($id)
    {
        $row = $this->Barang_keluar_model->get_by_id($id);
        $barang = $this->Barang_keluar_model->get_by_id_detail($id);
        if ($row) {
            $data = array(
                'id_barang_keluar' => $row->id_barang_keluar,
                'tgl_keluar' => $row->tgl_keluar,
                'keterangan_keluar' => $row->keterangan_keluar,
                'id_karyawan' => $row->id_karyawan,
                'id_user' => $row->id_user,
                'barang_data' => $barang,
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang_keluar/barang_keluar_read', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_keluar'));
        }
    }

    public function create($id_barang_keluar = '')
    {
        date_default_timezone_set("Asia/Jakarta");
        $id =  "OUT-" . date("ymd", strtotime(date('Y-m-d'))) . date('Hi');
        if ($id_barang_keluar == '' or $id_barang_keluar == null) {
            $data = array(
                'button' => 'Create',
                'action' => site_url('barang_keluar/create/' . $id),
                'id_barang_keluar' => $id_barang_keluar,
                'tgl_keluar' => set_value('tgl_keluar'),
                'keterangan_keluar' => set_value('keterangan_keluar'),
                'karyawan' => $this->db->get('tbl_karyawan'),
                'id_user' => set_value('id_user'),

            );
            $title = array(
                'title' => 'Detail',
            );

            $this->load->view('cover/header', $title);
            $this->load->view('barang_keluar/barang_keluar_form', $data);
            $this->load->view('cover/footer');
        } else {

            $barang = $this->Barang_model->get_all();
            $data = array(
                'button' => 'Create',
                'action' => site_url('barang_keluar/create_action'),
                'id_barang_keluar' => $id_barang_keluar,
                'tgl_keluar' => $this->input->post('tgl_keluar'),
                'keterangan_keluar' => $this->input->post('keterangan_keluar'),
                'id_karyawan' => $this->input->post('id_karyawan'),
                'karyawan' => $this->db->get('tbl_karyawan'),
                'barang_data' => $barang,
            );
            $title = array(
                'title' => 'Detail',
            );

            $this->load->view('cover/header', $title);
            $this->load->view('barang_keluar/barang_keluar_tambah', $data);
            $this->load->view('cover/footer');
        }
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            // $this->create();
            $data = array(
                'id_barang_keluar' => $this->input->post('id_keluar', TRUE),
                'tgl_keluar' => $this->input->post('tgl_keluar', TRUE),
                'keterangan_keluar' => $this->input->post('keterangan_keluar', TRUE),
                'id_karyawan' => $this->input->post('id_karyawan'),
                'id_user' => $this->session->userdata('id_user')
            );
            echo "<pre>";
            print_r($data);
        } else {
            $data = array(
                'id_barang_keluar' => $this->input->post('id_keluar', TRUE),
                'tgl_keluar' => $this->input->post('tgl_keluar', TRUE),
                'keterangan_keluar' => $this->input->post('keterangan_keluar', TRUE),
                'id_karyawan' => $this->input->post('id_karyawan', TRUE),
                'id_user' => $this->session->userdata('id_user')
            );

            $id_barang = $this->input->post('id_barang');
            $persediaan = $this->input->post('persediaan');
            $jumlah = $this->input->post('jumlah');

            $cek_jumlah = 0;
            foreach ($jumlah as $key) {
                $cek_jumlah = $cek_jumlah + $key;
            }

            if ($cek_jumlah <= 0) {
                echo "<script type='text/javascript'>alert('Submit Gagal!')</script>";
                redirect(site_url('barang_keluar'), 'refresh');
            }
            $this->db->trans_start();
            $this->Barang_keluar_model->insert($data);

            foreach ($id_barang as $key => $value) {
                if ($jumlah[$key] > 0) {
                    $insert_data = array(
                        'id_barang' => $id_barang[$key],
                        'jumlah_keluar' => $jumlah[$key],
                        'id_barang_keluar' => $this->input->post('id_keluar'),
                    );
                    $this->Pemakaian_model->insert($insert_data);

                    $persediaan = $this->Persediaan_model->get_by_id_barang($id_barang[$key]);
                    $hasil = 0;
                    foreach ($persediaan as $psd) {
                        if ($jumlah[$key] >= $psd->persediaan) {
                            $jumlah[$key] = $jumlah[$key] - $psd->persediaan;
                            $update = array('persediaan' => '0',);
                            $this->Persediaan_model->update($psd->id_persediaan, $update);
                        } else {
                            $hasil = $psd->persediaan - $jumlah[$key];
                            $update = array('persediaan' => $hasil,);
                            $this->Persediaan_model->update($psd->id_persediaan, $update);
                            $jumlah[$key] = 0;
                        }
                    }
                }
            }
            $this->db->trans_complete();
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('barang_keluar'));
        }
    }

    public function update($id)
    {
        $row = $this->Barang_keluar_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('barang_keluar/update_action'),
                'id_barang_keluar' => set_value('id_barang_keluar', $row->id_barang_keluar),
                'tgl_keluar' => set_value('tgl_keluar', $row->tgl_keluar),
                'keterangan_keluar' => set_value('keterangan_keluar', $row->keterangan_keluar),
                'karyawan' => $this->db->get('tbl_karyawan'),
                'id_user' => set_value('id_user', $row->id_user),
            );

            $title = array(
                'title' => 'Detail',
            );
            $this->load->view('cover/header', $title);
            $this->load->view('barang_keluar/barang_keluar_form', $data);
            $this->load->view('cover/footer');
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_keluar'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id_barang_keluar', TRUE));
        } else {
            $data = array(
                'tgl_keluar' => $this->input->post('tgl_keluar', TRUE),
                'keterangan_keluar' => $this->input->post('keterangan_keluar', TRUE),
                'id_karyawan' => $this->input->post('id_karyawan', TRUE),
            );

            $this->Barang_keluar_model->update($this->input->post('id_barang_keluar', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('barang_keluar'));
        }
    }

    public function delete($id)
    {
        $row = $this->Barang_keluar_model->get_by_id($id);

        if ($row) {
            $this->Barang_keluar_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('barang_keluar'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_keluar'));
        }
    }

    public function cetak()
    {
        $id = $this->input->post('id_keluar');
        $row = $this->Barang_keluar_model->get_by_id($id);
        $barang = $this->Barang_keluar_model->get_by_id_detail($id);
        if ($row) {
            $data = array(
                'id_barang_keluar' => $id,
                'tgl_keluar' => $row->tgl_keluar,
                'keterangan_keluar' => $row->keterangan_keluar,
                'id_karyawan' => $row->id_karyawan,
                'id_user' => $row->id_user,
                'laporan_data' => $barang,
            );
            $this->load->view('barang_keluar/laporan_keluar', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('barang_keluar'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('tgl_keluar', 'tgl keluar', 'trim|required');
        $this->form_validation->set_rules('keterangan_keluar', 'keterangan keluar', 'trim|required');
        $this->form_validation->set_rules('id_karyawan', 'id karyawan', 'trim|required');
        $this->form_validation->set_rules('id_user', 'id user', 'trim');

        $this->form_validation->set_rules('id_barang_keluar', 'id_barang_keluar', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "barang_keluar.xls";
        $judul = "barang_keluar";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Tgl Keluar");
        xlsWriteLabel($tablehead, $kolomhead++, "Keterangan Keluar");
        xlsWriteLabel($tablehead, $kolomhead++, "Id Karyawan");
        xlsWriteLabel($tablehead, $kolomhead++, "Id User");

        foreach ($this->Barang_keluar_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->tgl_keluar);
            xlsWriteLabel($tablebody, $kolombody++, $data->keterangan_keluar);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_karyawan);
            xlsWriteNumber($tablebody, $kolombody++, $data->id_user);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=barang_keluar.doc");

        $data = array(
            'barang_keluar_data' => $this->Barang_keluar_model->get_all(),
            'start' => 0
        );

        $this->load->view('barang_keluar/barang_keluar_doc', $data);
    }
    public function tes($value = '')
    {
        $this->load->view('cover/header');
        $this->load->view('barang_keluar/tes');
        $this->load->view('cover/footer');
    }
}
