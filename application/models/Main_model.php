<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_model extends CI_Model
{
    function stock()
    {        
        $sql="(SELECT barang,stok FROM
        (SELECT b.nama_barang barang ,c.limit  lmt,
        sum(a.persediaan) stok 
        FROM tbl_persediaan a inner join tbl_barang b 
        on a.id_barang=b.id_barang inner join tbl_kategori c 
        on b.id_kategori=c.id_kategori  
        group by a.id_barang) x where x.stok<=x.lmt)";
        $query = $this->db->query($sql);
        return $query->result();        
    }

    function keluar($tgl)
    {
        $this->db->select("COUNT(pk.id_barang) AS jumlah
            FROM (SELECT p.id_barang FROM tbl_pemakaian p JOIN tbl_barang_keluar k
                ON k.id_barang_keluar = p.id_barang_keluar
                WHERE k.tgl_keluar = '".$tgl."'
                GROUP BY p.id_barang) pk");
        return $this->db->get()->row();
    }

    function last_out()
    {
        $this->db->order_by('id_barang_keluar', 'desc');
        return $this->db->get('tbl_barang_keluar', 1)->row();
    }

    function last_nom()
    {
        $row = $this->last_out();
        $id = $row->id_barang_keluar;
        $this->db->select('count(id_barang) as jml');
        $this->db->from('tbl_pemakaian');
        $this->db->where('id_barang_keluar', $id);
        return $this->db->get()->row();
    }

    function laporan()
    {
        $this->db->select('month(tgl_masuk) as bulan, year(tgl_masuk) as tahun');
        $this->db->from('tbl_barang_masuk'); 
        $this->db->group_by('tahun, bulan');
        $query = $this->db->get(); 
        return $query->result();
    }

    public function lplpb($bln='', $thn='')
    {
        $this->db->select('o.nama_barang, coalesce(pnm.penerimaan,0) penerimaan , coalesce(pmk.pemakaian,0) pemakaian, coalesce(pmt.permintaan,0) permintaan, COALESCE(pnm.stok_awal, psd.persediaan_awal,0) stok_awal');
        $this->db->from('tbl_barang o');
        $this->db->join('(SELECT p.id_barang, SUM(p.jumlah_masuk) AS penerimaan, p.persediaan_awal AS stok_awal
            FROM tbl_persediaan p LEFT JOIN tbl_barang_masuk m ON(p.id_barang_masuk = m.id_barang_masuk) 
            WHERE YEAR(tgl_masuk) = '.$thn.' 
            AND MONTH(tgl_masuk) = '.$bln.' 
            GROUP BY p.id_barang) as pnm', 'pnm.id_barang = o.id_barang', 'left');
        $this->db->join('(SELECT p.id_barang, SUM(p.jumlah_keluar) AS pemakaian
            FROM tbl_pemakaian p LEFT JOIN tbl_barang_keluar k ON (p.id_barang_keluar = k.id_barang_keluar)
            WHERE YEAR(tgl_keluar) = '.$thn.'
            AND MONTH(tgl_keluar) = '.$bln.'
            GROUP BY p.id_barang) pmk', 'pmk.id_barang = o.id_barang', 'left');
        $this->db->join('(SELECT pd.id_barang, SUM(pd.jumlah_permintaan) AS permintaan 
            FROM tbl_permintaan_detail pd LEFT JOIN tbl_permintaan p ON(pd.id_permintaan = p.id_permintaan) 
            WHERE pd.status = "diterima" AND YEAR(tgl_permintaan) = '.$thn.' AND MONTH(tgl_permintaan) = '.$bln.'
            GROUP BY id_barang) pmt', 'pmt.id_barang = o.id_barang', 'left');
        $this->db->join('(SELECT id_barang, persediaan_awal FROM tbl_persediaan GROUP BY id_barang ORDER BY id_barang DESC) psd', 'psd.id_barang = o.id_barang', 'left');
        $this->db->group_by('o.id_barang');
        $query = $this->db->get(); 
        if($query->num_rows() != 0)
        {
            return $query->result();
        }
        else
        {
            return false;
        }
    }
}
