<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Permintaan_model extends CI_Model
{

    public $table = 'tbl_permintaan';
    public $id = 'id_permintaan';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function permintaan()
    {
        $this->db->select('month(tgl_permintaan) as bulan, year(tgl_permintaan) as tahun');
        $this->db->from('tbl_permintaan'); 
        $this->db->group_by('tahun, bulan');
        $query = $this->db->get(); 
        return $query->result();
    }

    function detail_permintaan_ka($bln, $thn)
    {
        $this->db->select('p.id_permintaan, p.tgl_permintaan, o.nama_barang, p.jumlah_permintaan,p.status, p.id_user');
        $this->db->from('tbl_permintaan p'); 
        $this->db->join('tbl_barang o', 'p.id_barang=o.id_barang', 'left');
        $this->db->where('month(tgl_permintaan)',$bln);
        $this->db->where('year(tgl_permintaan)',$thn);
        $query = $this->db->get(); 
        return $query->result();
    }

    function detail_permintaan_kp($bln, $thn)
    {
        $this->db->select('p.id_permintaan, p.tgl_permintaan, o.nama_barang, sum(pd.jumlah_permintaan),pd.status, p.id_user');
        $this->db->from('tbl_permintaan p'); 
        $this->db->join('tbl_permintaan_detail pd', 'pd.id_permintaan=p.id_permintaan', 'left');
        $this->db->join('tbl_barang o', 'pd.id_barang=o.id_barang', 'left');
        $this->db->where('month(tgl_permintaan)',$bln);
        $this->db->where('year(tgl_permintaan)',$thn);
        $this->db->where('pd.status', "diterima");
        $this->db->group_by('o.id_barang');
        $query = $this->db->get(); 
        return $query->result();
    }

    function detail_permintaan_kp_by_id($id)
    {
        $this->db->select('pd.id_permintaan, o.nama_barang, pd.jumlah_permintaan,pd.status');
        $this->db->from('tbl_permintaan_detail pd'); 
        $this->db->join('tbl_barang o', 'pd.id_barang=o.id_barang', 'left');
        $this->db->where('pd.id_permintaan',$id);
        $this->db->where('pd.status', "diterima");
        $query = $this->db->get(); 
        return $query->result();
    }


    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_id_detail($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get('tbl_permintaan_detail')->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id_permintaan', $q);
        $this->db->or_like('id_barang', $q);
        $this->db->or_like('tgl_permintaan', $q);
        $this->db->or_like('jumlah_permintaan', $q);
        $this->db->or_like('keterangan_permintaan', $q);
        $this->db->or_like('id_user', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_permintaan', $q);
        $this->db->or_like('id_barang', $q);
        $this->db->or_like('tgl_permintaan', $q);
        $this->db->or_like('jumlah_permintaan', $q);
        $this->db->or_like('keterangan_permintaan', $q);
        $this->db->or_like('id_user', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    function insert_detail($data)
    {
        $this->db->insert('tbl_permintaan_detail', $data);
    }

    function update_permintaan($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    } 

    // update data
    function update($id, $data)
    {
        $this->db->where('id_permintaan_detail', $id);
        $this->db->update('tbl_permintaan_detail', $data);
    }   

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function delete_detail($id)
    {
        $this->db->where('id_permintaan', $id);
        $this->db->delete('tbl_permintaan_detail');
    }
        
}