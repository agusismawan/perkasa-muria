<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Barang_keluar_model extends CI_Model
{

    public $table = 'tbl_barang_keluar';
    public $id = 'id_barang_keluar';
    public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    function get_by_id_detail($id)
    {
        $this->db->select('o.id_barang, k.nama_kategori, o.nama_barang, o.kode_barang, p.jumlah_keluar, p.id_pemakaian');
        $this->db->from('tbl_barang o');
        $this->db->join('tbl_pemakaian p', 'p.id_barang = o.id_barang', 'left');
        $this->db->join('tbl_kategori k', 'k.id_kategori = o.id_kategori', 'left');
        $this->db->where('p.id_barang_keluar', $id);
        $this->db->order_by('o.id_barang', $this->order);
        return $this->db->get()->result();
    }
    
    function total_rows($q = NULL) {
        $this->db->like('id_barang_keluar', $q);
    $this->db->or_like('tgl_keluar', $q);
    $this->db->or_like('keterangan_keluar', $q);
    $this->db->or_like('id_karyawan', $q);
    $this->db->or_like('id_user', $q);
    $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id_barang_keluar', $q);
    $this->db->or_like('tgl_keluar', $q);
    $this->db->or_like('keterangan_keluar', $q);
    $this->db->or_like('id_karyawan', $q);
    $this->db->or_like('id_user', $q);
    $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}