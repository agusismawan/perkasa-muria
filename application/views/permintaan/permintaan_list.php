<section class="content">
    <div class="row">
       <section class="col-lg-12 connectedSortable">
           <div class="box">
            <h2 style="margin-top:0px">Daftar Permintaan</h2>
            <div class="row" style="margin-bottom: 10px">
<!--                 <div class="col-md-6">
                    <?php echo anchor(site_url('permintaan/create'),'Create', 'class="btn btn-primary"'); ?>
                </div> -->
                <div class="col-md-6 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Permintaan</th>
                            <th>Tgl Permintaan</th>
                            <th>Keterangan Permintaan</th>
                            <th>Nama User</th>
                            <th>Jumlah Barang</th>
                            <th>Status</th>
                            <th><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody><?php
                    $start = 0;
                    foreach ($permintaan_data as $permintaan)
                    {
                       ?>

                       <tr>

                       <td width="80px"><?php echo ++$start ?></td>
                       <td><?php echo $permintaan->id_permintaan ?></td>
                       <td><?php echo $permintaan->tgl_permintaan ?></td>
                       <td><?php echo $permintaan->keterangan_permintaan ?></td>                       
                       <td><?php 
                       $this->db->where('id_user', $permintaan->id_user); 
                       echo $this->db->get('tbl_user',1)->row()->nama_user;
                       ?></td>
                       <td><?php 
                       $this->db->select('count(*) as jml');
                       $this->db->from('tbl_permintaan_detail');
                       $this->db->where('id_permintaan', $permintaan->id_permintaan);
                       echo $this->db->get()->row()->jml;
                       ?></td>
                       <td><?php
                       $this->db->where('id_permintaan', $permintaan->id_permintaan);
                       echo $this->db->get('tbl_permintaan_detail',1)->row()->status;
                       ?></td>
                       <td style="text-align:center" width="200px">
                        <input type="button" name="view" value="View" id="<?php echo $permintaan->id_permintaan; ?>" class="btn btn-info btn-sm view_data" />
                        <?php 
                        // echo anchor(site_url('permintaan/read/'.$permintaan->id_permintaan),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
                        // echo '  '; 
                        echo anchor(site_url('permintaan/update/'.$permintaan->id_permintaan),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
                        echo '  '; 
                        echo anchor(site_url('permintaan/delete/'.$permintaan->id_permintaan),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td>
                </tr> 
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
  </section>
</div>
<div id="view_permintaan" class="modal fade">  
      <div class="modal-dialog modal-lg">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">Detail Permintaan</h4>  
                </div>  
                <div class="modal-body" id="view_detail">  
                </div>  
                <div class="modal-footer">  
                     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                </div>  
           </div>  
      </div>  
 </div>
</section>  

