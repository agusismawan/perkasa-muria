<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tbl_permintaan List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Barang</th>
		<th>Tgl Permintaan</th>
		<th>Jumlah Permintaan</th>
		<th>Keterangan Permintaan</th>
		<th>Id User</th>
		
            </tr><?php
            foreach ($permintaan_data as $permintaan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $permintaan->id_barang ?></td>
		      <td><?php echo $permintaan->tgl_permintaan ?></td>
		      <td><?php echo $permintaan->jumlah_permintaan ?></td>
		      <td><?php echo $permintaan->keterangan_permintaan ?></td>
		      <td><?php echo $permintaan->id_user ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>