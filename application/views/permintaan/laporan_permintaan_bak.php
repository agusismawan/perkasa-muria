<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style_doc.css" type="text/css" />
<body onload="window.print();"> 
  <body> 
<?php
    error_reporting(0);
    // $dateObj   = DateTime::createFromFormat('!m', $bulan);
    // $bln = $dateObj->format('F');

    ?>
    <table width='80%' border='0'>
      <tr>
        <td align=center><b>LAPORAN PERMINTAAN</b></td>
      </tr>
    </table>
    <br/>
    <table width="80%" border = '0'>
      <col width="10%">
      <col width="20%">
      <col width="20%">
      <col width="25%">
      <col width="25%">
      <tr>
        <td align="left"> PROFOTEX</td>
        <td align="left"> : KUDUS</td>
        <td></td>
        <td align="left"> PERMINTAAN BULAN</td>
        <td align="left"> : <?php echo $bln." ".$tahun ?></td>
      </tr>
      <tr>
        <td align="left"> KAB/ KODYA</td>
        <td align="left"> : KUDUS</td>
        <td></td>
        <td align="left"> </td>
        <td align="left"> </td>
      </tr>

    </table>

    <br/>
    <table width='80%' border='1'>
     <thead>
      <tr>
       <th>NO</th>
       <th>NAMA BARANG</th>
       <th>SATUAN</th>
       <th>JUMLAH PERMINTAAN</th>
     </tr>
   </thead>   
   <tbody>

    <?php $no = 0; ?>
    <?php foreach ($laporan_data as $laporan): ?>


      <tr>
        <td><center><?php echo ++$no ?></center></td>
        <!-- <td><?php
        $ltgl = date("d", strtotime($laporan->tgl_permintaan));
        $lbln = date("m", strtotime($laporan->tgl_permintaan));
        $lthn = date("Y", strtotime($laporan->tgl_permintaan));
        $dateObj   = DateTime::createFromFormat('!m', $bulan);
        $tbln = $dateObj->format('F');
        echo $ltgl." ".$tbln." ".$lthn;
        ?>            
      </td> -->
      <td><?php echo $laporan->nama_barang ?></td>
      <td><?php echo $laporan->satuan ?></td>
      <td align="center"><?php echo $laporan->jumlah_permintaan ?></td>
      <!-- <td align="center">
        <?php 
        $this->db->where('id_user', $laporan->id_user);
        $this->db->from('tbl_user');
        echo $this->db->get()->row()->nama_user;
        ?>
      </td> -->
    </tr>
  <?php endforeach ?>		    				                                  
</tbody>							  
</table> 
<br/>
<br/>
<br/>
<table width='75%' border='0'>
  <tr>
    <th width='201' scope='col'>Issued By</th>
    <th width='202' scope='col'>Verified By</th>
    <th width='218' scope='col'>Approved By</th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
</tr>
<tr>
  <th width='201' scope='col'>(<?php echo $this->session->userdata('nama_user');; ?>)</th>
  <th width='202' scope='col'>(----------------------------) </th>
  <th width='218' scope='col'>(----------------------------)</th>

</tr>

</table>

</body> 				 
