
<section class="content">
  <div class="row">
   <section class="col-lg-12 connectedSortable">
     <div class="box">
      <?php  
      date_default_timezone_set("Asia/Jakarta");
      $id =  "REQ-".date("ymd", strtotime(date('Y-m-d'))).date('Hi');
      ?> 
      <form action="<?php echo $action ?>" method="post" accept-charset="utf-8">
        <h2 style="margin-left:20px">Form Permintaan Barang</h2>
        <h4 style="margin-left:20px">Kode Permintaan : <?php echo $id ?></h4>
         <h4 style="margin-left:20px"> <table width="40%"" border="0">
            <tr>
              <td>Tanggal Permintaan : </td>
              <td><input type="date" class="form-control" name="tgl_permintaan" value="<?php echo date('Y-m-d') ?>" /></td>
            </tr>
            <tr>
              <td>Keterangan Permintaan : </td>
              <td><input type="text" class="form-control" name="ket_permintaan" placeholder="Keterangan Permintaan" /></td>
            </tr>
          </table> </h4>
          <div class="row" style="margin-bottom: 10px">
            <div class="col-md-6 text-center">
              <div style="margin-top: 8px" id="message">
                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
              </div>
            </div>
          </div>
          <div class="box-body">
            <table class="table table-bordered table-striped" id="example1">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kategori</th>
                  <th>Nama Barang</th>
                  <th>Kode Barang</th>
                  <th>Persediaan</th>
                  <th>Jumlah Permintaan</th>
                </tr>
              </thead>
              <tbody><?php
              $start = 0;
              foreach ($barang_data as $barang)
              {
               ?>

               <tr>

                 <td width="40px"><?php echo ++$start ?></td>
                 <td><?php echo $barang->nama_kategori ?></td>
                 <td><?php echo $barang->nama_barang ?></td>
                 <td><?php echo $barang->kode_barang ?></td>
                 <td><?php echo $barang->persediaan ?></td>
                 <input type="hidden" name="id_barang[]" value="<?php echo $barang->id_barang ?>">
                 <td><input type="text" name="jumlah[]" placeholder="0" size="11" maxlength="11"></td>
               </tr> 
               <?php
             }
             ?>
           </tbody>
         </table>
         <input type="hidden" name="id_permintaan" value="<?php echo $id; ?>" />
         <button type="submit" class="btn btn-primary">Submit</button> 
         <a href="<?php echo site_url('permintaan') ?>" class="btn btn-default">Cancel</a>
       </form>
     </div>
   </section>
 </div>
</section>    

