<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style_doc.css" type="text/css" />
<body onload="window.print();"> 
  <body> 
<?php
    error_reporting(0);
    // $dateObj   = DateTime::createFromFormat('!m', $bulan);
    // $bln = $dateObj->format('F');

    ?>
    <table width='80%' border='0'>
      <tr>
        <td align=center><b>LAPORAN PERMINTAAN</b></td>
      </tr>
      <tr>
        <td align=center><b> PT. PUTRA PERKASA MURIA</b></td>
      </tr>
    </table>
    <br/>
    <table width="80%" border = '0'>
      <col width="10%">
      <col width="20%">
      <col width="20%">
      <col width="25%">
      <col width="25%">
      <tr>
        <td align="left"> KABUPATEN</td>
        <td align="left"> : KUDUS</td>
        <td></td>
        <td align="left"> PERMINTAAN TANGGAL</td>
        <td align="left"> : <?php $this->db->where('id_permintaan', $id);
        echo $this->db->get('tbl_permintaan')->row()->tgl_permintaan; ?></td>
      </tr>
      <tr>
        <td align="left"></td>
        <td align="left"></td>
        <td></td>
        <td align="left"> NO. PERMINTAAN</td>
        <td align="left"> : <?php echo $id ?></td>
      </tr>

    </table>

    <br/>
    <table width='80%' border='1'>
     <thead>
      <tr>
       <th>NO</th>
       <th>NAMA BARANG</th>       
       <th>JUMLAH PERMINTAAN</th>
     </tr>
   </thead>   
   <tbody>

    <?php $no = 0; ?>
    <?php foreach ($laporan_data as $laporan): ?>


      <tr>
        <td><center><?php echo ++$no ?></center></td>
    
      <td><?php echo $laporan->nama_barang ?></td>
      <td align="center"><?php echo $laporan->jumlah_permintaan ?></td>
    </tr>
  <?php endforeach ?>		    				                                  
</tbody>							  
</table> 
<br/>
<br/>
<br/>
<table width='75%' border='0'>
  <tr>
    <th width='201' scope='col'>Diperiksa Oleh</th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'>Disetujui Oleh</th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>

  </tr>
</tr>
<tr>
  <th width='201' scope='col'>(<?php echo $this->session->userdata('nama_user');; ?>)</th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'>(Pimpinan)</th>

</tr>

</table>

</body> 				 
