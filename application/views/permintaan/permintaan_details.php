

<section class="content">
    <div class="row">
     <section class="col-lg-12 connectedSortable">
         <div class="box">
            <h2 style="margin-top:0px">Permintaan List</h2>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-6 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tgl Permintaan</th>
                            <th>Nama Barang</th>
                            <th>Jumlah Permintaan</th>
                            <th>Id User</th>
                            <th>Status Permintaan</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody><?php
                    $start = 0;
                    foreach ($permintaan_data as $permintaan)
                    {
                     ?>

                     <tr>

                        <td width="80px"><?php echo ++$start ?></td>
                        <td><?php echo $permintaan->tgl_permintaan ?></td>
                        <td><?php echo $permintaan->nama_barang ?></td>
                        <td><?php echo $permintaan->jumlah_permintaan ?></td>
                        <td><?php echo $permintaan->id_user ?></td>
                        <td><?php echo $permintaan->status ?></td>
                        <td style="text-align:center" width="200px">
                            <?php 
                            echo anchor(site_url('permintaan/read/'.$permintaan->id_permintaan),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
                            echo '  '; 
                            echo anchor(site_url('permintaan/update/'.$permintaan->id_permintaan),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
                            echo '  '; 
                            echo anchor(site_url('permintaan/delete/'.$permintaan->id_permintaan),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                            ?>
                        </td>
                    </tr> 
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6">
            <form action= "<?php echo site_url('permintaan/cetak') ?>" method="post" accept-charset="utf-8" target="_blank">
                <input type="hidden" name="bulan" value="<?php echo $bulan ?>" /> 
                <input type="hidden" name="tahun" value="<?php echo $tahun ?>" /> 
                <button type="submit" class="btn btn-primary">Cetak</button>
            </form>
        </div>
    </section>
</div>
</section>    

