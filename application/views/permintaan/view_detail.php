<?php  
 if(isset($_POST["permintaan_id"]))  
 {  
      $this->db->where('id_permintaan', $_POST["permintaan_id"]);
      $result = $this->db->get('tbl_permintaan_detail')->result();
      echo '  
      <div class="table-responsive" id="status_tab">  
           <table class="table table-bordered">
           <th width= "50%">Nama Barang </th>
           <th width= "10%">Stok Barang </th>
           <th width= "10%">Permintaan </th>
           <th width= "10%">Status </th>
           ';
      if ($this->session->userdata('role') == "Pimpinan") {
        echo '<th width = "20%">Action </th>';
      }
 
      foreach($result as $key)  
      {  
           echo '  
                <tr>  
                     <td>';
                          $this->db->where('id_barang', $key->id_barang);
                          echo $this->db->get('tbl_barang')->row()->nama_barang;
            echo '</td><td>';
                         $this->db->select('sum(persediaan) as stok');
                         $this->db->where('id_barang', $key->id_barang);
                         echo $this->db->get('tbl_persediaan')->row()->stok;
            echo '</td><td>'.
                          $key->jumlah_permintaan
                     .'</td><td>'.$key->status.'</td>   
           ';
           if ($this->session->userdata('role') == "Pimpinan") {
               if($key->status == "menunggu"){
                 echo '<td><button type="button"  idk="'.$key->id_permintaan_detail.'" class="btn btn-success btn-sm edit_status" title="diterima" onclick="javasciprt: return confirm(\'Are You Sure ?\')  ">
                  <span class="glyphicon glyphicon-ok" ></span> </button>
                ';
                echo " | ";
                echo '<button type="button"  idk="'.$key->id_permintaan_detail.'" class="btn btn-danger btn-sm edit_status" title="ditolak" onclick="javasciprt: return confirm(\'Are You Sure ?\')  ">
                  <span class="glyphicon glyphicon-remove" ></span> 
                </td>';
               } else if ($key->status == "diterima") {
                 echo '<td><a href="#" class="btn btn-success btn-sm disabled" role="button" aria-disabled="true">
                  <span class="glyphicon glyphicon-ok" ></span> </a>
                </td>'; } else {
                  echo '<td><a href="#" class="btn btn-danger btn-sm disabled" role="button" aria-disabled="true">
                  <span class="glyphicon glyphicon-remove" ></span> </a>
                </td>';
               }
             } else if ($key->status == "diterima") {
                 echo '<td><a href="#" class="btn btn-success btn-sm disabled" role="button" aria-disabled="true">
                  <span class="glyphicon glyphicon-ok" ></span> </a>
                </td>'; } else if ($key->status == "ditolak"){
                  echo '<td><a href="#" class="btn btn-danger btn-sm disabled" role="button" aria-disabled="true">
                  <span class="glyphicon glyphicon-remove" ></span> </a>
                </td>'; } else {
                  echo '<td><a href="#" class="btn btn-info btn-sm disabled" role="button" aria-disabled="true">
                  <span class="glyphicon glyphicon glyphicon-hourglass" ></span> </a>
                </td>';
               }
      echo '</tr>';  
      }  
      echo '  
           </table>  
      </div>  
      ';
      echo '<div class="col-md-6">
        <form action= "'.site_url('permintaan/cetak').'" method="post" accept-charset="utf-8" target="_blank">
            <input type="hidden" name="id_permintaan" value="'.$_POST["permintaan_id"].'" /> 
            <button type="submit" class="btn btn-primary">Cetak</button>
        </form>

    </div>';  
 }  
 ?>
 