
        <section class="content">
        <div class="row">
         <section class="col-lg-12 connectedSortable">
         <div class="box">
        <h2 style="margin-top:0px">Edit Permintaan</h2>
         <div class="box-body">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="date">Tgl Permintaan <?php echo form_error('tgl_permintaan') ?></label>
            <input type="text" class="form-control" name="tgl_permintaan" id="tgl_permintaan" placeholder="Tgl Permintaan" value="<?php echo $tgl_permintaan; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Keterangan Permintaan <?php echo form_error('keterangan_permintaan') ?></label>
            <input type="text" class="form-control" name="keterangan_permintaan" id="keterangan_permintaan" placeholder="Keterangan Permintaan" value="<?php echo $keterangan_permintaan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Nama User/Pengaju <?php echo form_error('id_user') ?></label>
            <?php 
                $this->db->where('id_user', $id_user);
                $user = $this->db->get('tbl_user')->row()->nama_user;
             ?>
            <input type="text" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?php echo $user; ?>" disabled />
        </div>
	    <input type="hidden" name="id_permintaan" value="<?php echo $id_permintaan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('permintaan') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>

        </div>
    </section>
    </div>
    </section>    
    