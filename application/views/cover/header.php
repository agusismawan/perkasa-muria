<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>PT. PUTRA PERKASA MURIA</title>
  <link rel="icon" href="<?php echo base_url(); ?>temp/dist/img/logohana.png" type="image/png" />

  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/dist/css/skins/_all-skins.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>temp/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="<?php echo base_url() ?>" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><img src="<?php echo base_url(); ?>temp/dist/img/logohana.png" alt="Logo" width="50px"></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg" style="font-size: 17px"><small><b>PT. PUTRA PERKASA MURIA</b></small></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo base_url(); ?>temp/dist/img/<?php echo $this->session->userdata('foto'); ?>" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $this->session->userdata('username'); ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="<?php echo base_url(); ?>temp/dist/img/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image">

                  <p>
                    <?php echo $this->session->userdata('nama_user'); ?>
                    <small><?php echo $this->session->userdata('role'); ?></small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-right">
                    <a href="<?php echo base_url() . 'Auth/logout'; ?>" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
            <!-- Control Sidebar Toggle Button -->
            <li>
              <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="<?php echo base_url(); ?>temp/dist/img/<?php echo $this->session->userdata('foto'); ?>" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo $this->session->userdata('nama_user'); ?></p>
            <small class="text-info"><?php echo 'Login as : ' . $this->session->userdata('role'); ?></small>
          </div>
        </div>
        <ul class="sidebar-menu">
          <li role="separator" class="divider"></li>
          <li class="header"></li>
          <li class="active treeview">
            <a href="<?php echo base_url() . 'Main/'; ?>">
              <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-table"></i> <span>Data Barang</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if ($this->session->userdata('role') != 'Pimpinan') : ?>
                <li><a href="<?php echo base_url() . 'barang/create'; ?>"><i class="fa fa-circle-o"></i> <span>Tambah Barang</span></a></li>
              <?php endif ?>
              <li><a href="<?php echo base_url() . 'barang'; ?>"><i class="fa fa-circle-o"></i> <span>List Data Barang</span></a></li>
            </ul>
          </li>
          <?php if ($this->session->userdata('role') != 'Kepala Gudang') : ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-truck"></i> <span>Barang Masuk</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if ($this->session->userdata('role') == 'Administrator') : ?>
                  <li><a href="<?php echo base_url() . 'barang_masuk/create'; ?>"><i class="fa fa-circle-o"></i> <span>Tambah Barang Masuk</span></a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() . 'barang_masuk'; ?>"><i class="fa fa-circle-o"></i> <span>List Barang Masuk</span></a></li>
              </ul>
            </li>
          <?php endif ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-exchange"></i> <span>Barang Keluar</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if ($this->session->userdata('role') == 'Kepala Gudang') : ?>
                <li><a href="<?php echo base_url() . 'barang_keluar/create'; ?>"><i class="fa fa-circle-o"></i> <span>Tambah Barang Keluar</span></a></li>
              <?php endif ?>
              <li><a href="<?php echo base_url() . 'barang_keluar'; ?>"><i class="fa fa-circle-o"></i> <span>List Barang Keluar</span></a></li>
            </ul>
          </li>
          <?php if ($this->session->userdata('role') != 'Kepala Gudang') : ?>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-recycle"></i> <span>Permintaan</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <?php if ($this->session->userdata('role') != 'Pimpinan') : ?>
                  <li><a href="<?php echo base_url() . 'permintaan/create'; ?>"><i class="fa fa-circle-o"></i> <span>Tambah Permintaan</span></a></li>
                <?php endif ?>
                <li><a href="<?php echo base_url() . 'permintaan'; ?>"><i class="fa fa-circle-o"></i> <span>List Permintaan</span></a></li>
              </ul>
            </li>
          <?php endif ?>
          <?php if ($this->session->userdata('role') != 'Kepala Gudang') : ?>
            <?php if ($this->session->userdata('role') != 'Kepala Gudang') : ?>
              <li><a href="<?php echo base_url() . 'user' ?>"><i class="fa fa-users"></i> <span>Data User</span></a></li>
            <?php endif ?>
            <li><a href="<?php echo base_url() . 'main/laporan' ?>"><i class="fa fa-print text-aqua"></i> <span>Laporan LPLPB</span></a></li>
          <?php endif ?>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">