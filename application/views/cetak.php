<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style_doc.css" type="text/css" />
<body onload="window.print();"> 
  <?php
  error_reporting(0);
 $dateObj   = DateTime::createFromFormat('!m', $bulan);
$bln = $dateObj->format('F');
$dateObj   = DateTime::createFromFormat('!m', $bulan+1);
$n_bln = $dateObj->format('F');
  ?>
  <table width='100%' border='0'>
    <tr>
      <td align=center><b>LAPORAN PEMAKAIAN DAN LEMBAR PERMINTAAN BARANG</b></td>
    </tr>
    <tr>
        <td align=center><b> PT. PUTRA PERKASA MURIA</b></td>
      </tr>
  </table>
  <br/>
  <table width="100%" border = '0'>
    <col width="10%">
    <col width="20%">
    <col width="20%">
    <col width="27%">
    <col width="23%">
    <tr>
      <td align="left"> KABUPATEN</td>
      <td align="left"> : KUDUS</td>
      <td></td>
      <td align="left"> PELAPORAN BULAN/PERIODE</td>
      <td align="left"> : <?php echo $bln." ".$tahun; ?></td>
    </tr>
    <tr>
      <td align="left"></td>
      <td align="left"></td>
      <td></td>
      <td align="left"> PERMINTAAN BULAN/ PERIODE</td>
      <td align="left"> : <?php echo $n_bln.' '; if ($bulan+1 > 12) echo $tahun+1; else echo $tahun;  ?></td>
    </table>

    <br/>
    <table width='100%' border='1'>
     <thead>
      <tr>
       <th>NO</th>
       <th>NAMA BARANG</th>
       <th>STOK AWAL</th>
       <th>PENERIMAAN</th>
       <th>PERSEDIAAN</th>
       <th>PEMAKAIAN</th>
       <th>SISA STOK</th>
       <th>PERMINTAAN</th>
     </tr>
   </thead>   
   <tbody>

    <?php $no = 0; ?>
    <?php foreach ($laporan_data as $laporan): ?>


      <tr>
        <td><center><?php echo ++$no ?></center></td>
        <td><?php echo $laporan->nama_barang ?></td>
        <td align="center"><?php echo $laporan->stok_awal ?></td>
        <td align="center"><?php echo $laporan->penerimaan ?></td>
        <td align="center"><?php echo $persediaan = ($laporan->stok_awal + $laporan->penerimaan) ?></td>
        <td align="center"><?php echo $laporan->pemakaian ?></td>
        <td align="center"><?php echo $persediaan - $laporan->pemakaian ?></td>
        <td align="center"><?php echo $laporan->permintaan ?></td>

      </tr>
    <?php endforeach ?>		    				                                  
  </tbody>							  
</table> 
<br/>
<br/>
<br/>
<table width='95%' border='0'>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'>Disetujui</th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr><tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
  <tr>
    <th width='201' scope='col'></th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'></th>
    
  </tr>
</tr>
<tr>
  <th width='201' scope='col'></th>
  <th width='202' scope='col'></th>
  <th width='218' scope='col'>Pimpinan</th>

</tr>

</table>

</body> 				 
