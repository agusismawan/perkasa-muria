<section class="content">
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <div class="box">
                <h2 style="margin-top:0px">Tambah Data Barang</h2>
                <div class="box-body">
                    <form action="<?php echo $action; ?>" method="post">
                        <div class="form-group">
                            <label for="varchar">Nama Barang <?php echo form_error('nama_barang') ?></label>
                            <input type="text" class="form-control" name="nama_barang" id="nama_barang" placeholder="Nama Barang" value="<?php echo $nama_barang; ?>" />
                        </div>
                        <div class="form-group">
                            <label for="int">Kategori : </label>
                            <select name="id_kategori" class="form-control">
                                <?php
                                foreach ($kategori->result() as $key) {
                                    echo '<option value="' . $key->id_kategori . '">' . $key->nama_kategori . '</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="varchar">Kode Barang <?php echo form_error('kode_barang') ?></label>
                            <input type="text" class="form-control" name="kode_barang" id="kode_barang" placeholder="Kode Barang" value="<?php echo $kode_barang; ?>" />
                        </div>
                        <input type="hidden" name="id_barang" value="<?php echo $id_barang; ?>" />
                        <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                        <a href="<?php echo site_url('barang') ?>" class="btn btn-default">Cancel</a>
                    </form>
                </div>

            </div>
        </section>
    </div>
</section>