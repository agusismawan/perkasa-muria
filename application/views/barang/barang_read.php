<section class="content">
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <div class="box">
                <h2 style="margin-top:0px">Detail Data Barang</h2>
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <td>Id Kategori</td>
                            <td><?php echo $id_kategori; ?></td>
                        </tr>
                        <tr>
                            <td>Nama Barang</td>
                            <td><?php echo $nama_barang; ?></td>
                        </tr>
                        <tr>
                            <td>Kode Barang</td>
                            <td><?php echo $kode_barang; ?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><a href="<?php echo site_url('barang') ?>" class="btn btn-default">Cancel</a></td>
                        </tr>
                    </table>
                </div>

            </div>
        </section>
    </div>
</section>