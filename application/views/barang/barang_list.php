<section class="content">
    <div class="row">
        <section class="col-lg-12 connectedSortable">
            <div class="box">
                <h2 style="margin-top:0px">Daftar Barang</h2>
                <div class="row" style="margin-bottom: 10px">
                    <div class="col-md-6 text-center">
                        <div style="margin-top: 8px" id="message">
                            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                        </div>
                    </div>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="example1">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Nama Barang</th>
                                <th>Kode Barang</th>
                                <th>Persediaan</th>
                                <th>
                                    <center>Action</center>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $start = 0;
                            foreach ($barang_data as $barang) {
                            ?>
                                <tr>

                                    <td width="40px"><?php echo ++$start ?></td>
                                    <td><?php echo $barang->nama_kategori ?></td>
                                    <td><?php echo $barang->nama_barang ?></td>
                                    <td><?php echo $barang->kode_barang ?></td>
                                    <td><?php echo $barang->persediaan ?></td>
                                    <td style="text-align:center" width="100px">
                                        <button type="button" name="view" value="View" id="<?php echo $barang->id_barang; ?>" class="btn btn-default btn-sm view_barang"><i class="fa fa-eye"></i></button>
                                        <?php
                                        echo '  ';
                                        echo anchor(site_url('barang/update/' . $barang->id_barang), '<i class="fa fa-pencil-square-o"></i>', array('title' => 'edit', 'class' => 'btn btn-warning btn-sm'));
                                        echo '  ';
                                        ?>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
        </section>
        <div id="view_barang_modal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Detail Barang</h4>
                    </div>
                    <div class="modal-body" id="view_detail_barang">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
</section>