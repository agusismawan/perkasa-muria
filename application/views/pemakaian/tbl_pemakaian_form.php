
        <section class="content">
        <div class="row">
         <section class="col-lg-12 connectedSortable">
         <div class="box">
        <h2 style="margin-top:0px">Pemakaian <?php echo $button ?></h2>
         <div class="box-body">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Id Persediaan <?php echo form_error('id_persediaan') ?></label>
            <input type="text" class="form-control" name="id_persediaan" id="id_persediaan" placeholder="Id Persediaan" value="<?php echo $id_persediaan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Jumlah Keluar <?php echo form_error('jumlah_keluar') ?></label>
            <input type="text" class="form-control" name="jumlah_keluar" id="jumlah_keluar" placeholder="Jumlah Keluar" value="<?php echo $jumlah_keluar; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Id Barang Keluar <?php echo form_error('id_barang_keluar') ?></label>
            <input type="text" class="form-control" name="id_barang_keluar" id="id_barang_keluar" placeholder="Id Barang Keluar" value="<?php echo $id_barang_keluar; ?>" />
        </div>
	    <input type="hidden" name="id_pemakaian" value="<?php echo $id_pemakaian; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('pemakaian') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>

        </div>
    </section>
    </div>
    </section>    
    