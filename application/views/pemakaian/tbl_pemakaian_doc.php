<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tbl_pemakaian List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Persediaan</th>
		<th>Jumlah Keluar</th>
		<th>Id Barang Keluar</th>
		
            </tr><?php
            foreach ($pemakaian_data as $pemakaian)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $pemakaian->id_persediaan ?></td>
		      <td><?php echo $pemakaian->jumlah_keluar ?></td>
		      <td><?php echo $pemakaian->id_barang_keluar ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>