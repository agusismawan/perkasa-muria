

<section class="content">
    <div class="row">
       <section class="col-lg-12 connectedSortable">
           <div class="box">
            <h2 style="margin-left:20px">Form Barang Masuk</h2>
            <h5 style="margin-left:20px">ID Barang Masuk : <?php echo $id_barang_masuk ?></h5>
                <h5 style="margin-left:20px">Tanggal Barang Masuk : <?php echo date("d-m-Y", strtotime($tgl_masuk)) ?></h5>
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-md-6 text-center">
                            <div style="margin-top: 8px" id="message">
                                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo $action ?>" method="post" accept-charset="utf-8">
                            <table class="table table-bordered table-striped" id="example1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Nama Barang</th>
                                        <th>Kode Barang</th>
                                        <!-- <th>Satuan</th> -->
                                        <th>Jumlah</th>
                                        <!-- <th>Expired</th> -->
                                    </tr>
                                </thead>
                                <tbody><?php
                                $start = 0;
                                foreach ($barang_data as $barang)
                                {
                                   ?>

                                   <tr>

                                     <td width="40px"><?php echo ++$start ?></td>
                                     <td><?php echo $barang->nama_kategori ?></td>
                                     <td><?php echo $barang->nama_barang ?></td>
                                     <td><?php echo $barang->kode_barang ?></td>
                                     <!-- <td><?php echo $barang->satuan ?></td> -->
                                     <input type="hidden" name="id_barang[]" value="<?php echo $barang->id_barang ?>">
                                     <input type="hidden" name="persediaan[]" value="<?php echo $barang->persediaan ?>">
                                     <td><input type="text" name="jumlah[]" placeholder="0" size="11" maxlength="11"></td>
                                     <!-- <td style="text-align:center"><input type="date" name="expired[]" value="<?php echo date('Y-m-d') ?>"></td> -->
                         <!-- <td>
                            <?php 
                            echo anchor(site_url('barang/read/'.$barang->id_barang),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
                            echo '  '; 
                            echo anchor(site_url('barang/update/'.$barang->id_barang),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
                            echo '  '; 
                            echo anchor(site_url('barang/delete/'.$barang->id_barang),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                            ?>
                        </td> -->
                    </tr> 
                    <?php
                }
                ?>
            </tbody>
        </table>
        <input type="hidden" name="id_masuk" value="<?php echo $id_barang_masuk; ?>" />
        <input type="hidden" name="tgl_masuk" value="<?php echo $tgl_masuk; ?>" />
        <input type="hidden" name="keterangan_masuk" value="<?php echo $keterangan_masuk; ?>" />
        <button type="submit" class="btn btn-primary">Submit</button> 
        <a href="<?php echo site_url('barang_masuk') ?>" class="btn btn-default">Cancel</a>
    </form>
</div>
</section>
</div>
</section>    

