

<section class="content">
  <div class="row">
   <section class="col-lg-12 connectedSortable">
     <div class="box">
      <h2 style="margin-top:0px"> Daftar Barang Masuk</h2>
      <div class="row" style="margin-bottom: 10px">
        <div class="col-md-6 text-center">
          <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          </div>
        </div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
          <thead>
            <tr>
              <th style="text-align:center">No</th>
              <th>ID Barang Masuk</th>
              <th>Tgl Masuk</th>
              <th>Keterangan Masuk</th>
              <th>Id User</th>
              <th style="text-align:center">Action</th>
            </tr>
          </thead>
          <tbody><?php
          $start = 0;
          foreach ($barang_masuk_data as $barang_masuk)
          {
           ?>

           <tr>

             <td width="40px"><?php echo ++$start ?></td>
             <td><?php echo $barang_masuk->id_barang_masuk ?></td>
             <td><?php echo $barang_masuk->tgl_masuk ?></td>
             <td><?php echo $barang_masuk->keterangan_masuk ?></td>
             <td><?php 
                $this->db->where('id_user', $barang_masuk->id_user);
                echo $this->db->get('tbl_user')->row()->nama_user;
              ?></td>
             <td style="text-align:center" width="200px">
              <?php 
              echo anchor(site_url('barang_masuk/read/'.$barang_masuk->id_barang_masuk),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
              echo '  '; 
              echo anchor(site_url('barang_masuk/update/'.$barang_masuk->id_barang_masuk),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
              echo '  '; 
              ?>
            </td>
          </tr> 
          <?php
        }
        ?>
      </tbody>
    </table>
  </div>
  </section>
</section>    

