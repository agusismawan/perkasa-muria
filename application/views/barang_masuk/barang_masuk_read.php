

<section class="content">
    <div class="row">
       <section class="col-lg-12 connectedSortable">
           <div class="box">
            <h2 style="margin-top:0px">Barang Masuk Detail</h2>
            <div class="box-body">
                <table class="table">
                   <tr><td>Tgl Masuk</td><td><?php echo $tgl_masuk; ?></td></tr>
                   <tr><td>Keterangan Masuk</td><td><?php echo $keterangan_masuk; ?></td></tr>
                   <tr><td>Id User</td><td><?php echo $id_user; ?></td></tr>
                   <tr><td></td><td><a href="<?php echo site_url('barang_masuk') ?>" class="btn btn-default">Cancel</a></td></tr>
               </table>
           </div>
       </div>
       <div class="box">
        <h2 style="margin-top:0px">List Barang Masuk</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-6 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <table class="table table-bordered table-striped" id="example1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Kategori</th>
                        <th>Nama Barang</th>
                        <th>Kode Barang</th>
                        <th>Jumlah</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody><?php
                $start = 0;
                foreach ($barang_data as $barang)
                {
                   ?>

                   <tr>

                     <td width="40px"><?php echo ++$start ?></td>
                     <td><?php echo $barang->nama_kategori ?></td>
                     <td><?php echo $barang->nama_barang ?></td>
                     <td><?php echo $barang->kode_barang ?></td>
                     <td><?php echo $barang->jumlah_masuk ?></td>
                     <td style="text-align:center" width="200px">
                        <?php 
                        echo anchor(site_url('barang/read/'.$barang->id_barang),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
                        echo '  '; 
                        // echo anchor(site_url('persediaan/update/'.$barang->id_persediaan),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
                        // echo '  '; 
                        echo anchor(site_url('persediaan/delete/'.$barang->id_persediaan),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
                        ?>
                    </td>
                </tr> 
                <?php
            }
            ?>
        </tbody>
    </table>
</div>
<div class="row">
    <div class="col-md-6">
        <form action= "<?php echo site_url('barang_masuk/cetak') ?>" method="post" accept-charset="utf-8" target="_blank">
            <input type="hidden" name="id_masuk" value="<?php echo $id_barang_masuk ?>" /> 
            <button type="submit" class="btn btn-primary">Cetak</button>
        </form>

    </div>
</section>
</div>
</section>    

