
<section class="content">
    <div class="row">
     <section class="col-lg-12 connectedSortable">
         <div class="box">
            <h2 style="margin-top:0px">Tambah Barang Masuk</h2>
            <div class="box-body">
                <form action="<?php echo $action ?>" method="post">
                    <div class="form-group">
                        <label for="date">Tgl Masuk <?php echo form_error('tgl_masuk') ?></label>
                        <input type="date" class="form-control" name="tgl_masuk" id="tgl_masuk" placeholder="Tgl Masuk" value="<?php echo date('Y-m-d') ?>" />
                    </div>
                    <div class="form-group">
                        <label for="varchar">Keterangan Masuk <?php echo form_error('keterangan_masuk') ?></label>
                        <input type="text" class="form-control" name="keterangan_masuk" id="keterangan_masuk" placeholder="Keterangan Masuk" value="<?php echo $keterangan_masuk; ?>" />
                    </div>
                    <div class="form-group">
                        <label for="int">Nama User <?php echo form_error('id_user') ?></label>
                        <input type="text" class="form-control" name="id_user" id="id_user" placeholder="<?php echo $this->session->userdata('nama_user'); ?>" value="<?php echo $this->session->userdata('nama_user');; ?>" readonly />
                    </div>
                    <input type="hidden" name="id_barang_masuk" value="<?php echo $id_barang_masuk; ?>" />
                    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                    <a href="<?php echo site_url('barang_masuk') ?>" class="btn btn-default">Cancel</a>
                </form>
            </div>

        </div>
    </section>
</div>
</section>    
