<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style_doc.css" type="text/css" />
<body onload="window.print();"> 
  <body> 
    <?php
    error_reporting(0);
    $tgl = date("d", strtotime($tgl_keluar));
    $bulan = date("m", strtotime($tgl_keluar));
    $tahun = date("Y", strtotime($tgl_keluar));

    $this->db->where('id_user', $id_user);
    $this->db->from('tbl_user');
    $user = $this->db->get()->row();
 
  $dateObj   = DateTime::createFromFormat('!m', $bulan);
  $bln = $dateObj->format('F');

    ?>
    <table width='100%' border='0'>
      <tr>
        <td align=center><b>LAPORAN PEMAKAIAN</b></td>
      </tr>
      <tr>
        <td align=center><b> PT. PUTRA PERKASA MURIA</b></td>
      </tr>
    </table>
    <br/>
    <table width="100%" border = '0'>
      <col width="10%">
      <col width="20%">
      <col width="25%">
      <col width="22%">
      <col width="23%">
      <tr>
        <td align="left">KABUPATEN</td>
        <td align="left"> : KUDUS</td>
        <td></td>
        <td align="left"> TANGGAL PEMAKAIAN</td>
        <td align="left"> : <?php echo $tgl." ".$bln." ".$tahun ?></td>
      </tr>
      <tr>
        <td align="left">KETERANGAN</td>
        <td align="left"> : <?php echo $keterangan_keluar ?></td>
        <td></td>
        <td align="left"> NO PEMAKAIAN</td>
        <td align="left"> : <?php echo $id_barang_keluar ?></td>
      </tr>
      <tr>
        <td align="left"></td>
        <td align="left"></td>
        <td></td>
        <td align="left"> PENANGGUNG JAWAB</td>
        <td align="left"> : <?php echo $user->nama_user." (NIP. ".$user->nip.")" ?></td>
      </tr>
      </table>

      <br/>
      <table width='100%' border='1'>
       <thead>
        <tr>
         <th>NO</th>
         <th>KATEGORI</th>
         <th>NAMA BARANG</th>
         <th>KODE BARANG</th>
         <!-- <th>SATUAN</th> -->
         <th>JUMLAH</th>
       </tr>
     </thead>   
     <tbody>

      <?php $no = 0; ?>
      <?php foreach ($laporan_data as $laporan): ?>


        <tr>
          <td><center><?php echo ++$no ?></center></td>
          <td><?php echo $laporan->nama_kategori ?></td>
          <td><?php echo $laporan->nama_barang ?></td>
          <td align="center"><?php echo $laporan->kode_barang ?></td>
          <!-- <td align="center"><?php echo $laporan->satuan ?></td> -->
          <td align="center"><?php echo $laporan->jumlah_keluar ?></td>
        </tr>
      <?php endforeach ?>		    				                                  
    </tbody>							  
  </table> 
  <br/>
  <br/>
  <br/>
  <table width='95%' border='0'>
    <tr>
      <th width='201' scope='col'>Diperiksa Oleh</th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'>Disetujui Oleh</th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr><tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr><tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
    <tr>
      <th width='201' scope='col'></th>
      <th width='202' scope='col'></th>
      <th width='218' scope='col'></th>

    </tr>
  </tr>
  <tr>
    <th width='201' scope='col'>(<?php echo $this->session->userdata('nama_user');; ?>)</th>
    <th width='202' scope='col'></th>
    <th width='218' scope='col'>(Pimpinan)</th>

  </tr>

</table>

</body> 				 
