<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Barang keluar</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Tgl Keluar</th>
		<th>Keterangan Keluar</th>
        <th>Id Karyawan</th>
		<th>Id User</th>
		
            </tr><?php
            foreach ($barang_keluar_data as $barang_keluar)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $barang_keluar->tgl_keluar ?></td>
		      <td><?php echo $barang_keluar->keterangan_keluar ?></td>
              <td><?php echo $barang_keluar->id_karyawan ?></td>
		      <td><?php echo $barang_keluar->id_user ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>