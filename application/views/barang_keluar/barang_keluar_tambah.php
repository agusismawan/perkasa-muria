

<section class="content">
    <div class="row">
     <section class="col-lg-12 connectedSortable">
         <div class="box">
            <h2 style="margin-left:20px">Form Barang Keluar</h2>
            <h5 style="margin-left:20px">ID Barang Keluar : <?php echo $id_barang_keluar ?></h3>
                <h5 style="margin-left:20px">Tanggal Barang Keluar : <?php echo date("d-m-Y", strtotime($tgl_keluar)) ?></h3>
                    <div class="row" style="margin-bottom: 10px">
                        <div class="col-md-6 text-center">
                            <div style="margin-top: 8px" id="message">
                                <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                            </div>
                        </div>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo $action ?>" method="post" accept-charset="utf-8">
                            <table class="table table-bordered table-striped" id="example1">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Kategori</th>
                                        <th>Nama Barang</th>
                                        <th>Kode Barang</th>
                                        <th>Persediaan</th>
                                        <th>Jumlah</th>
                                    </tr>
                                </thead>
                                <tbody><?php
                                $start = 0;
                                foreach ($barang_data as $barang)
                                {
                                 ?>

                                 <tr>

                                   <td width="40px"><?php echo ++$start ?></td>
                                   <td><?php echo $barang->nama_kategori ?></td>
                                   <td><?php echo $barang->nama_barang ?></td>
                                   <td><?php echo $barang->kode_barang ?></td>
                                   <td><?php echo $barang->persediaan ?></td>
                                   <input type="hidden" name="id_barang[]" value="<?php echo $barang->id_barang ?>">
                                   <input type="hidden" name="persediaan[]" value="<?php echo $barang->persediaan ?>">
                                   <td><input type="text" name="jumlah[]" placeholder="0" size="11" maxlength="11"></td>
                               </tr> 
                               <?php
                           }
                           ?>
                       </tbody>
                   </table>
                   <input type="hidden" name="id_keluar" value="<?php echo $id_barang_keluar; ?>" />
                   <input type="hidden" name="tgl_keluar" value="<?php echo $tgl_keluar; ?>" />
                   <input type="hidden" name="id_karyawan" value="<?php echo $id_karyawan; ?>" />
                   <input type="hidden" name="keterangan_keluar" value="<?php echo $keterangan_keluar; ?>" />
                   <button type="submit" class="btn btn-primary">Submit</button> 
                   <a href="<?php echo site_url('barang_keluar') ?>" class="btn btn-default">Cancel</a>
               </form>
           </div>
       </section>
   </div>
</section>    

