
<section class="content">
    <div class="row">
     <section class="col-lg-12 connectedSortable">
         <div class="box">
            <h2 style="margin-top:0px">Tambah Barang Keluar</h2>
            <div class="box-body">
                <form action="<?php echo $action ?>" method="post">
                 <div class="form-group">
                    <label for="date">Tgl Keluar <?php echo form_error('tgl_keluar') ?></label>
                    <input type="date" class="form-control" name="tgl_keluar" id="tgl_keluar" placeholder="Tgl Keluar" value="<?php echo date('Y-m-d') ?>" />
                </div>
                <div class="form-group">
                    <label for="varchar">Keterangan Keluar <?php echo form_error('keterangan_keluar') ?></label>
                    <input type="text" class="form-control" name="keterangan_keluar" id="keterangan_keluar" placeholder="Keterangan Keluar" value="<?php echo $keterangan_keluar; ?>" />
                </div>               
                <div class="form-group">
                <label for="int">Nama Karyawan</label>
                <select name="id_karyawan" class="form-control">
                   <?php 
                    foreach ($karyawan->result() as $key) {
                        echo '<option value="'.$key->id_karyawan.'">'.$key->nama_karyawan.'</option>';
                    }
                    ?>
                </select>
                </div>
                <div class="form-group">
                    <label for="int">Id User <?php echo form_error('id_user') ?></label>
                    <input type="text" class="form-control" name="id_user" id="id_user" placeholder="<?php echo $this->session->userdata('nama_user'); ?>" value="<?php echo $this->session->userdata('nama_user'); ?>" readonly />
                </div>
                <input type="hidden" name="id_barang_keluar" value="<?php echo $id_barang_keluar; ?>" /> 
                <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                <a href="<?php echo site_url('barang_keluar') ?>" class="btn btn-default">Cancel</a>
            </form>
        </div>

    </div>
</section>
</div>
</section>    
