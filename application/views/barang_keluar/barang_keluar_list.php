<section class="content">
    <div class="row">
     <section class="col-lg-12 connectedSortable">
         <div class="box">
            <h2 style="margin-top:0px"> Daftar Barang Keluar</h2>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-6 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>ID Barang Keluar</th>
                            <th>Tgl Keluar</th>
                            <th>Keterangan Keluar</th>
                            <th>Nama Karyawan</th>
                            <th>User</th>
                            <th>Jumlah Barang</th>
                            <th><center>Action</center></th>
                        </tr>
                    </thead>
                    <tbody><?php
                    $start = 0;
                    foreach ($barang_keluar_data as $barang_keluar)
                    {
                     ?>

                     <tr>

                       <td width="80px"><?php echo ++$start ?></td>
                       <td><?php echo $barang_keluar->id_barang_keluar ?></td>
                       <td><?php echo $barang_keluar->tgl_keluar ?></td>
                       <td><?php echo $barang_keluar->keterangan_keluar ?></td>
                       <td><?php $this->db->where('id_karyawan', $barang_keluar->id_karyawan);
                       echo $this->db->get('tbl_karyawan')->row()->nama_karyawan; ?></td>
                       <td><?php $this->db->where('id_user', $barang_keluar->id_user);
                       echo $this->db->get('tbl_user')->row()->nama_user; ?></td>
                       <td><?php 
                       $this->db->select('count(*) as jml');
                       $this->db->from('tbl_pemakaian');
                       $this->db->where('id_barang_keluar', $barang_keluar->id_barang_keluar);
                       echo $this->db->get()->row()->jml;
                       ?></td>
                       <td style="text-align:center" width="100px">
                        <?php 
                        echo anchor(site_url('barang_keluar/read/'.$barang_keluar->id_barang_keluar),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
                        echo '  '; 
                        echo anchor(site_url('barang_keluar/update/'.$barang_keluar->id_barang_keluar),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
                        echo '  '; 
                        ?>
                    </td>
                </tr> 
                <?php
            }
            ?>
        </tbody>
    </table>
</div>
</section>
</section>    

