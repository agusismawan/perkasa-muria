<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Tbl_persediaan List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Id Barang</th>
		<!-- <th>Expired</th> -->
		<th>Persediaan Awal</th>
		<th>Persediaan</th>
		<th>Jumlah Masuk</th>
		<th>Id Barang Masuk</th>
		
            </tr><?php
            foreach ($persediaan_data as $persediaan)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $persediaan->id_barang ?></td>
		      <!-- <td><?php echo $persediaan->expired ?></td> -->
		      <td><?php echo $persediaan->persediaan_awal ?></td>
		      <td><?php echo $persediaan->persediaan ?></td>
		      <td><?php echo $persediaan->jumlah_masuk ?></td>
		      <td><?php echo $persediaan->id_barang_masuk ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>