
        <section class="content">
        <div class="row">
         <section class="col-lg-12 connectedSortable">
         <div class="box">
        <h2 style="margin-top:0px">Persediaan <?php echo $button ?></h2>
         <div class="box-body">
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Id Barang <?php echo form_error('id_barang') ?></label>
            <input type="text" class="form-control" name="id_barang" id="id_barang" placeholder="Id Barang" value="<?php echo $id_barang; ?>" />
        </div>
	    <!-- <div class="form-group">
            <label for="date">Expired <?php echo form_error('expired') ?></label>
            <input type="text" class="form-control" name="expired" id="expired" placeholder="Expired" value="<?php echo $expired; ?>" />
        </div> -->
	    <div class="form-group">
            <label for="int">Persediaan Awal <?php echo form_error('persediaan_awal') ?></label>
            <input type="text" class="form-control" name="persediaan_awal" id="persediaan_awal" placeholder="Persediaan Awal" value="<?php echo $persediaan_awal; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Persediaan <?php echo form_error('persediaan') ?></label>
            <input type="text" class="form-control" name="persediaan" id="persediaan" placeholder="Persediaan" value="<?php echo $persediaan; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Jumlah Masuk <?php echo form_error('jumlah_masuk') ?></label>
            <input type="text" class="form-control" name="jumlah_masuk" id="jumlah_masuk" placeholder="Jumlah Masuk" value="<?php echo $jumlah_masuk; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Id Barang Masuk <?php echo form_error('id_barang_masuk') ?></label>
            <input type="text" class="form-control" name="id_barang_masuk" id="id_barang_masuk" placeholder="Id Barang Masuk" value="<?php echo $id_barang_masuk; ?>" />
        </div>
	    <input type="hidden" name="id_persediaan" value="<?php echo $id_persediaan; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('persediaan') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div>

        </div>
    </section>
    </div>
    </section>    
    