
        
        <section class="content">
        <div class="row">
         <section class="col-lg-12 connectedSortable">
         <div class="box">
        <h2 style="margin-top:0px"> List Persediaan</h2>
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-6">
                <?php echo anchor(site_url('persediaan/create'),'Create', 'class="btn btn-primary"'); ?>
            </div>
            <div class="col-md-6 text-center">
                <div style="margin-top: 8px" id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <div class="box-body">
        <table class="table table-bordered table-striped" id="example1">
            <thead>
            <tr>
                <th>No</th>
		<th>Id Barang</th>
		<!-- <th>Expired</th> -->
		<th>Persediaan Awal</th>
		<th>Persediaan</th>
		<th>Jumlah Masuk</th>
		<th>Id Barang Masuk</th>
		<th>Action</th>
                            </tr>
                            </thead>
                            <tbody><?php
                            $start = 0;
                            foreach ($persediaan_data as $persediaan)
                            {
                                 ?>
            
            <tr>
            
			<td width="80px"><?php echo ++$start ?></td>
			<td><?php echo $persediaan->id_barang ?></td>
			<!-- <td><?php echo $persediaan->expired ?></td> -->
			<td><?php echo $persediaan->persediaan_awal ?></td>
			<td><?php echo $persediaan->persediaan ?></td>
			<td><?php echo $persediaan->jumlah_masuk ?></td>
			<td><?php echo $persediaan->id_barang_masuk ?></td>
			<td style="text-align:center" width="200px">
				<?php 
				echo anchor(site_url('persediaan/read/'.$persediaan->id_persediaan),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
				echo '  '; 
				echo anchor(site_url('persediaan/update/'.$persediaan->id_persediaan),'<i class="fa fa-pencil-square-o"></i>',array('title'=>'edit','class'=>'btn btn-warning btn-sm')); 
				echo '  '; 
				echo anchor(site_url('persediaan/delete/'.$persediaan->id_persediaan),'<i class="fa fa-trash-o"></i>','title="delete" class="btn btn-danger btn-sm" onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
				?>
			</td>
		</tr> 
                <?php
            }
            ?>
        </tbody>
        </table>
        </div>
        <div class="row">
 
    </section>
    </div>
    </section>    
    
   