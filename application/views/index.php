
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Beranda
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Beranda</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h5>
                <?php echo $last_id ."<br>".$last_jml ?>
              </h5>

              <p>Barang Keluar Terakhir</p>
            </div>
            <div class="icon">
              <i class="ion-android-send"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3><?php echo $keluar ?></sup></h3>

              <p>Barang Keluar Hari Ini</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          </div>
        </div>        
      </div>
      <!-- /.row -->

      
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <div class="box">
            <div class="box-body">
          <!-- small box -->
          <!-- <div class="small-box bg-red"> -->
            <!-- <div class="inner"> -->
              <table class="table table-bordered table-striped">
                <h4>Barang Stok Menipis</h4>
                <thead>
                  <tr>
                    <th>Nama Barang</th>
                    <th>Sisa Stok</th>
                  </tr>
                </thead>
                <tbody><?php                    
                    foreach ($stok as $min_data)
                    {
                       ?>
                  <tr>
                    <td><?php echo $min_data->barang ?></td>
                    <td><?php echo $min_data->stok ?></td>
                  </tr>
                  <?php
                }
                ?>
                </tbody>   
            </table>
            <!-- </div>            -->
            <!--<a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>-->
          <!-- </div> -->
        </div>
        </div>        

        </section>
        <!-- /.Left col -->


        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-5 connectedSortable">


        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  