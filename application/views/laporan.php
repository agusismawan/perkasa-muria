

<section class="content">
  <div class="row">
   <section class="col-lg-12 connectedSortable">
     <div class="box">
      <h2 style="margin-top:0px"> Laporan Permintaan dan Penerimaan Barang</h2>
      <div class="row" style="margin-bottom: 10px">
        <div class="col-md-6 text-center">
          <div style="margin-top: 8px" id="message">
            <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
          </div>
        </div>
      </div>
      <div class="container" style="width:500px;">  
        <h3 align="center">Pilih Periode/Bulan Laporan</h3>  
        <br />  
        <div class="table-responsive">  
            <br />  
          <form method="post" id="periode" action="<?php echo base_url() ?>main/cetak" target="_blank">  
            <table class="table table-bordered">  
             <tr>  
              <th width="25%">Bulan : </th>  
              <th width="25%">
                <?php 
                $monthArray = range(1, 12);
                $formattedMonthArray = array(
                  "1" => "Januari", "2" => "Februari", "3" => "Maret", "4" => "April",
                  "5" => "Mei", "6" => "Juni", "7" => "Juli", "8" => "Agustus",
                  "9" => "September", "10" => "Oktober", "11" => "November", "12" => "Desember",
                );
                ?>
                <select name="bulan">
                  <option value="">Select Month</option>
                  <?php
                  foreach ($monthArray as $month) {
        // if you want to select a particular month
                    $selected = ($month == date('m')) ? 'selected' : '';
        // if you want to add extra 0 before the month uncomment the line below
        //$month = str_pad($month, 2, "0", STR_PAD_LEFT);
                    echo '<option '.$selected.' value="'.$month.'">'.$formattedMonthArray[$month].'</option>';
                  }
                  ?>
                </select>


              </th>  
              <th width="25%">Tahun :</th>  
              <th width="25%">
                <?php 
                $yearArray = range(2000, 2050);
                ?>
                <select name="tahun">
                  <option value="">Select Year</option>
                  <?php
                  foreach ($yearArray as $year) {
        // if you want to select a particular year
                    $selected = ($year == date('Y')) ? 'selected' : '';
                    echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
                  }
                  ?>
                </select>              


              </th>
              <th>
                <button type="submit" name="pilih" id="pilih" class="btn btn-warning">Search</button>
                <!-- <?php echo anchor(site_url('main/cetak'), 'Word', 'class="btn btn-warning"'); ?> -->
              </th>  
            </tr>  
        </table>  
      </form>  
    </div>  
  </div>
  
  <!-- <div class="box-body">
    <table class="table table-bordered table-striped" id="example1">
      <thead>
        <tr>
          <th>No</th>
          <th>Tahun</th>
          <th>Bulan</th>
        </tr>
      </thead>
      <tbody><?php
      $start = 0;
      foreach ($list_laporan as $laporan)
      {
       ?>

       <tr>

         <td width="80px"><?php echo ++$start ?></td>
         <td><?php echo $laporan->tahun ?></td>
         <td><?php echo $laporan->bulan ?></td>
         <td style="text-align:center" width="200px">
          <?php 
          echo anchor(site_url('main/details/'.$laporan->tahun."/".$laporan->bulan),'<i class="fa fa-eye"></i>',array('title'=>'detail','class'=>'btn btn-default btn-sm')); 
          echo '  '; 
          ?>
        </td>
      </tr> 
      <?php
    }
    ?>
  </tbody>
</table>
</div> -->
  <!-- <div class="col-md-6">

    <?php echo anchor(site_url('permintaan/word'), 'Word', 'class="btn btn-primary"'); ?>

  </div> -->
</section>
</div>
</section>    

