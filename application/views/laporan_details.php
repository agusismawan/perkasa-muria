<?php 
$dateObj   = DateTime::createFromFormat('!m', $bulan);
$bln = $dateObj->format('F'); ?>
<section class="content">
    <div class="row">
       <section class="col-lg-12 connectedSortable">
           <div class="box">            
            <h2 style="margin-left:20px">Tabel Laporan </h2>
            <h4 style="margin-left:20px"> Bulan : <?php echo $bulan ?> <br>Tahun : <?php echo $tahun ?></h4>
            <div class="row" style="margin-bottom: 10px">
                <div class="col-md-6 text-center">
                    <div style="margin-top: 8px" id="message">
                        <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                    </div>
                </div>
            </div>
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Barang</th>
                            <!-- <th>Satuan</th> -->
                            <th>Penerimaan</th>
                            <th>Pemakaian</th>
                            <th>Permintaan</th>
                            <th>Stok Awal</th>
                            <th>Persediaan</th>
                            <th>Sisa Stok</th>
                        </tr>
                    </thead>
                    <tbody><?php
                    $start = 0;
                    $bln = $bulan;
                    $thn = $tahun;
                    foreach ($laporan_data as $laporan)
                    {
                       ?>

                       <tr>

                        <td width="80px"><?php echo ++$start ?></td>
                        <td><?php echo $laporan->nama_barang ?></td>
                        <!-- <td><?php echo $laporan->satuan ?></td> -->
                        <td><?php echo $laporan->penerimaan ?></td>
                        <td><?php echo $laporan->pemakaian ?></td>
                        <td><?php echo $laporan->permintaan ?></td>
                        <td><?php echo $laporan->stok_awal ?></td>
                        <td><?php echo $laporan->persediaan ?></td>
                        <td><?php echo $laporan->sisa_stok ?></td>
                        </td>
                    </tr> 
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6">
           <form action= "<?php echo site_url('main/cetak') ?>" method="post" accept-charset="utf-8">
            <input type="hidden" name="tahun" value="<?php echo $thn ?>" /> 
            <input type="hidden" name="bulan" value="<?php echo $bln ?>" /> 
            <?php echo anchor(site_url('main/excel/'.$tahun.'/'.$bulan), 'Excel', 'class="btn btn-primary"'); ?>
            <?php echo anchor(site_url('permintaan/word'), 'Word', 'class="btn btn-primary"'); ?>
            <?php echo anchor(site_url('main/cetak'), 'Print', 'class="btn btn-primary"'); ?>
            <button type="submit" class="btn btn-primary">Cetak</button>
        </form>

    </div>
</section>
</div>
</section>    

