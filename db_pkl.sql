-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 06:54 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pkl`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
  `id_barang` int(3) NOT NULL,
  `id_kategori` int(3) NOT NULL,
  `nama_barang` varchar(200) NOT NULL,
  `kode_barang` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `id_kategori`, `nama_barang`, `kode_barang`) VALUES
(1, 1, 'Tapping Connector', '6CUR-648'),
(2, 4, 'Suspension Clamp Bracket', '6CUR-638'),
(3, 2, 'Service Wedge Clamp', '5ELS'),
(4, 8, 'Turn Buckle', '8JUS70'),
(5, 3, 'Tention Bracket', 'TWFX'),
(6, 3, 'Cable Schoen', 'K5046'),
(7, 5, 'Fuse Holder', 'K6261');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang_keluar`
--

CREATE TABLE IF NOT EXISTS `tbl_barang_keluar` (
  `id_barang_keluar` varchar(15) NOT NULL,
  `tgl_keluar` date DEFAULT NULL,
  `keterangan_keluar` varchar(255) DEFAULT NULL,
  `id_karyawan` int(11) NOT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang_keluar`
--

INSERT INTO `tbl_barang_keluar` (`id_barang_keluar`, `tgl_keluar`, `keterangan_keluar`, `id_karyawan`, `id_user`) VALUES
('OUT-1912182057', '2019-12-18', 'Keluar pertama', 1, 52);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang_masuk`
--

CREATE TABLE IF NOT EXISTS `tbl_barang_masuk` (
  `id_barang_masuk` varchar(15) NOT NULL,
  `tgl_masuk` date DEFAULT NULL,
  `keterangan_masuk` varchar(255) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang_masuk`
--

INSERT INTO `tbl_barang_masuk` (`id_barang_masuk`, `tgl_masuk`, `keterangan_masuk`, `id_user`) VALUES
('IN-1912182053', '2019-12-18', 'Baru', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_karyawan`
--

CREATE TABLE IF NOT EXISTS `tbl_karyawan` (
  `id_karyawan` int(11) NOT NULL,
  `nama_karyawan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_karyawan`
--

INSERT INTO `tbl_karyawan` (`id_karyawan`, `nama_karyawan`) VALUES
(1, 'Hana'),
(2, 'Anisa'),
(3, 'Male'),
(4, 'Female'),
(5, 'Sonia'),
(6, 'Shella'),
(7, 'Yayang'),
(8, 'Anggraheni');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_kategori`
--

CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `id_kategori` int(4) NOT NULL,
  `nama_kategori` varchar(50) NOT NULL,
  `limit` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_kategori`
--

INSERT INTO `tbl_kategori` (`id_kategori`, `nama_kategori`, `limit`) VALUES
(1, 'Tiang', 5),
(2, 'Isolator', 5),
(3, 'Penghantar', 5),
(4, 'Travers/Cross Arm', 10),
(5, 'Kabel', 5),
(6, 'Saklar', 5),
(7, 'Screw', 5),
(8, 'Transistor', 5),
(9, 'Torong', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pemakaian`
--

CREATE TABLE IF NOT EXISTS `tbl_pemakaian` (
  `id_pemakaian` int(11) NOT NULL,
  `id_barang` int(3) DEFAULT NULL,
  `jumlah_keluar` int(11) DEFAULT NULL,
  `id_barang_keluar` varchar(15) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pemakaian`
--

INSERT INTO `tbl_pemakaian` (`id_pemakaian`, `id_barang`, `jumlah_keluar`, `id_barang_keluar`) VALUES
(1, 1, 5, 'OUT-1912182057');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permintaan`
--

CREATE TABLE IF NOT EXISTS `tbl_permintaan` (
  `id_permintaan` varchar(15) NOT NULL,
  `tgl_permintaan` date DEFAULT NULL,
  `keterangan_permintaan` varchar(255) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permintaan`
--

INSERT INTO `tbl_permintaan` (`id_permintaan`, `tgl_permintaan`, `keterangan_permintaan`, `id_user`) VALUES
('REQ-1912182113', '2019-12-18', 'Permintaan Mendesak', 2),
('REQ-1912182305', '2019-12-18', 'Permintaan dari Agus', 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permintaan_detail`
--

CREATE TABLE IF NOT EXISTS `tbl_permintaan_detail` (
  `id_permintaan_detail` int(11) NOT NULL,
  `id_barang` int(3) DEFAULT NULL,
  `jumlah_permintaan` int(11) DEFAULT NULL,
  `status` enum('diterima','ditolak','menunggu') DEFAULT NULL,
  `id_permintaan` varchar(15) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_permintaan_detail`
--

INSERT INTO `tbl_permintaan_detail` (`id_permintaan_detail`, `id_barang`, `jumlah_permintaan`, `status`, `id_permintaan`) VALUES
(1, 2, 10, 'diterima', 'REQ-1912182113'),
(2, 3, 5, 'ditolak', 'REQ-1912182113'),
(3, 6, 5, 'menunggu', 'REQ-1912182305');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_persediaan`
--

CREATE TABLE IF NOT EXISTS `tbl_persediaan` (
  `id_persediaan` int(11) NOT NULL,
  `id_barang` int(3) DEFAULT NULL,
  `persediaan_awal` int(11) DEFAULT NULL,
  `jumlah_masuk` int(11) DEFAULT NULL,
  `persediaan` int(11) DEFAULT NULL,
  `id_barang_masuk` varchar(15) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_persediaan`
--

INSERT INTO `tbl_persediaan` (`id_persediaan`, `id_barang`, `persediaan_awal`, `jumlah_masuk`, `persediaan`, `id_barang_masuk`) VALUES
(1, 1, 0, 20, 15, 'IN-1912182053'),
(2, 2, 0, 10, 10, 'IN-1912182053'),
(3, 3, 0, 10, 10, 'IN-1912182053'),
(4, 4, 0, 10, 10, 'IN-1912182053'),
(5, 5, 0, 10, 10, 'IN-1912182053'),
(6, 6, 0, 10, 10, 'IN-1912182053'),
(7, 7, 0, 10, 10, 'IN-1912182053');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id_user` int(5) NOT NULL,
  `nip` varchar(25) NOT NULL,
  `nama_user` varchar(100) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(100) NOT NULL,
  `role` enum('Pimpinan','Administrator','Kepala Gudang') NOT NULL,
  `foto` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `nip`, `nama_user`, `username`, `password`, `role`, `foto`) VALUES
(1, '960117181', 'Pimpinan', 'pimpinan', '59335c9f58c78597ff73f6706c6c8fa278e08b3a', 'Pimpinan', 'default.jpg'),
(2, '930716111', 'Administrator', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'Administrator', 'default.jpg'),
(52, '131755974', 'Gudang', 'gudang', 'a80dd043eb5b682b4148b9fc2b0feabb2c606fff', 'Kepala Gudang', 'default.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  ADD PRIMARY KEY (`id_barang`),
  ADD KEY `id_kategori` (`id_kategori`);

--
-- Indexes for table `tbl_barang_keluar`
--
ALTER TABLE `tbl_barang_keluar`
  ADD PRIMARY KEY (`id_barang_keluar`),
  ADD KEY `id_obat` (`tgl_keluar`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tbl_barang_masuk`
--
ALTER TABLE `tbl_barang_masuk`
  ADD PRIMARY KEY (`id_barang_masuk`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tbl_karyawan`
--
ALTER TABLE `tbl_karyawan`
  ADD PRIMARY KEY (`id_karyawan`);

--
-- Indexes for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `tbl_pemakaian`
--
ALTER TABLE `tbl_pemakaian`
  ADD PRIMARY KEY (`id_pemakaian`),
  ADD KEY `id_obat_keluar` (`id_barang_keluar`),
  ADD KEY `id_obat` (`id_barang`);

--
-- Indexes for table `tbl_permintaan`
--
ALTER TABLE `tbl_permintaan`
  ADD PRIMARY KEY (`id_permintaan`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `tbl_permintaan_detail`
--
ALTER TABLE `tbl_permintaan_detail`
  ADD PRIMARY KEY (`id_permintaan_detail`),
  ADD KEY `id_obat` (`id_barang`),
  ADD KEY `id_permintaan` (`id_permintaan`);

--
-- Indexes for table `tbl_persediaan`
--
ALTER TABLE `tbl_persediaan`
  ADD PRIMARY KEY (`id_persediaan`),
  ADD KEY `id_obat` (`id_barang`),
  ADD KEY `id_obat_masuk` (`id_barang_masuk`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
  MODIFY `id_barang` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_kategori`
--
ALTER TABLE `tbl_kategori`
  MODIFY `id_kategori` int(4) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tbl_pemakaian`
--
ALTER TABLE `tbl_pemakaian`
  MODIFY `id_pemakaian` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_permintaan_detail`
--
ALTER TABLE `tbl_permintaan_detail`
  MODIFY `id_permintaan_detail` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_persediaan`
--
ALTER TABLE `tbl_persediaan`
  MODIFY `id_persediaan` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
